# Airport Manager

Airport Manager is a multi-module Spring application that manages flight records at an airport. The system allows users to enter records about stewards, airplanes, and destinations, as well as update and delete these records. It also allows recording flights and managing the assignment of stewards to flights.

## Modules

The project is divided into the following modules:

- `reporter`: Generates reports for flights, providing flight details such as departure delay, arrival delay and flight detail description.
  - `download` function allows to create file including reports meeting arrival or/and departure delay conditions.
  - located on port 8083
- `messenger`: Handles notifications related to flight events, such as flight creation, update, or cancellation, and steward assignment or removal.
  - located on port 8082
- `resource-management`: Manages information about airplanes (capacity, name, type) and stewards (first name, last name), and handles CRUD operations for both resources.
  - located on port 8084
- `flight-management`: Manages flight records, ensures plane availability and schedules, assigns and removes stewards to/from flights, and handles CRUD operations for destinations (country, city).
  - located on port 8081

## Use Case Diagram
![Use Case Diagram](/Documentation/UseCase_diagram.png/)

## Class Diagram
![Class Diagram](/Documentation/class_diagram.png/)

## Getting Started

### Prerequisites

- JDK 17 or later
- Maven 3.6.x or later

### Building the project

1. Clone the repository:
```bash
git clone https://github.com/your_username/airport-manager.git
```

2. Navigate to the project directory:
```bash
cd airport-manager
```

3. Build the project using Maven:
```bash
mvn clean install
```

### Running the application
To run each module individually, navigate to the module's directory and execute the following command:
1. Clone the repository:
```bash
mvn spring-boot:run
```

### Running the tests
To run the unit tests and integration tests for the entire project, execute the following command in the project root directory:
1. Clone the repository:
```bash
mvn test
```

To run tests for a specific module, navigate to the module's directory and execute the mvn test command.

### Swagger
You can find swagger for any of the 4 services at `http://localhost:PORT/swagger-ui/index.html#/` 

### Docker / Podman
note: tested on Podman with podman-compose, feel free to replace with docker-compose

To build the images run this command in airport-manager directory
```
mvn clean install
podman-compose up --build
```

To run the images after building in detached mode run
```
podman-compose up -d
```
### Grafana
http://localhost:3000/

Username: admin 

Password: admin


### Runable Scenario

There are 3 runable scenarios:
1. `flightScenario` Get informations about all destinations and change flight name with id = 7 to "Lacov let" 
2. `reporterScenario` Create new reporter status and update delays for reporter with id = 5
3. `staffScenario` Delete plane with id = 11 and update last name of staff with id 4 to "Smith"

Install requirements for python3:
```
pip3 install locust
```

Run script by command:
```
locust -f {scenario}.py
```

### Security

Disclaimer: Confidential client is from Seminar 9 and is here only to obtain access token\
First gain access token by running confidential client like so

```
cd confidential-client
mvn spring-boot:run
```
- Go to http://localhost:8080/ or http://localhost:8080/login 
- log in
- go to http://localhost:8080/accesstoken
- copy the access token
```
curl -v -X 'GET' 'http://localhost:8084/api/resource-management/planes/id/0' -H 'accept: application/json' -H 'Authorization: Bearer TODOReplaceWithAccessToken'
curl -v -X 'GET' 'http://localhost:8081/api/flight/' -H 'accept: application/json' -H 'Authorization: Bearer TODOReplaceWithAccessToken'
curl -v -X 'GET' 'http://localhost:8082/api/messages/to-address/xyz' -H 'accept: application/json' -H 'Authorization: Bearer TODOReplaceWithAccessToken'
curl -v -X 'GET' 'http://localhost:8083/api/reporter' -H 'accept: application/json' -H 'Authorization: Bearer TODOReplaceWithAccessToken'
```