package cz.muni.fi.pa165.resource.repository;

import cz.muni.fi.pa165.resource.entity.Staff;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StaffRepository extends JpaRepository<Staff, Long> {}
