package cz.muni.fi.pa165.resource.controller;


import cz.muni.fi.pa165.model.dto.DestinationDto;
import cz.muni.fi.pa165.model.dto.PlaneDto;
import cz.muni.fi.pa165.resource.entity.Plane;
import cz.muni.fi.pa165.resource.mapper.PlaneMapper;
import cz.muni.fi.pa165.resource.service.ResourceService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/api/resource-management/planes")
public class PlaneController {

    private final ResourceService resourceService;
    private final PlaneMapper planeMapper;
    @Autowired
    public PlaneController(ResourceService resourceService, PlaneMapper planeMapper) {
        this.resourceService = resourceService;
        this.planeMapper = planeMapper;
    }

    @Operation(summary = "Get all planes", description = "Gets all planes from the system.")
    @GetMapping
    public ResponseEntity<?> getAllPlanes(Pageable pageable){
        try {
            Page<Plane> allPlanes = resourceService.getAllPlanes(pageable);
            return getListPlaneEntity(allPlanes);

        } catch (Exception exception) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Failed to list all planes: "+exception.getMessage());
        }
    }

    @Operation(summary = "Get plane by id", description = "Gets a plane by id from the system.")
    @GetMapping({"/id/{id}"})
    @Parameter(description = "Id of the plane")
    public ResponseEntity<?> getPlaneById(@PathVariable("id") Long planeId) {
        try {
            Plane plane = resourceService.getPlaneById(planeId);
            return ResponseEntity.ok(planeMapper.toDto(plane));
        } catch (Exception exception) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Failed to get plane: "+exception.getMessage());
        }
    }

    @Operation(summary = "Create a plane", description = "Creates a plane from the request body. Ignores Id")
    @PostMapping
    public ResponseEntity<PlaneDto> createPlane(@Valid @RequestBody @Parameter(description = "Plane details") PlaneDto plane) {
        try {
            return ResponseEntity.ok(planeMapper.toDto(resourceService.createPlane(planeMapper.toEntity(plane))));
        } catch (Exception exception) {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body(plane);
        }
    }

    @Operation(summary = "Update a plane", description = "Creates a plane from the request body. Uses Id")
    @PutMapping
    public ResponseEntity<PlaneDto> updatePlane(@Valid @RequestBody @Parameter(description = "Plane details") PlaneDto plane) {
        try {
            return ResponseEntity.ok(planeMapper.toDto(resourceService.updatePlane(planeMapper.toEntity(plane))));
        } catch (NoSuchElementException e) {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body(plane);
        }
    }

    @Operation(summary = "Delete plane by Id", description = "Deletes a plane by id if it exists in the system.")
    @DeleteMapping({"/{id}"})
    public ResponseEntity<String> removePlane(@Valid @PathVariable("id") @Parameter(description = "Plane id") Long id) {
        resourceService.removePlane(id);
        return ResponseEntity.ok("Removed the plane with Id " + id + " if it existed");
    }

    private ResponseEntity<List<PlaneDto>> getListPlaneEntity(Page<Plane> planes) {
        if (planes.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        List<PlaneDto> result = new ArrayList<>();
        for (var plane : planes) {
            result.add(planeMapper.toDto(plane));
        }
        return ResponseEntity.ok(result);
    }

}

