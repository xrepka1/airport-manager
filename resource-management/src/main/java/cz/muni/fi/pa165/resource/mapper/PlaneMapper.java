package cz.muni.fi.pa165.resource.mapper;

import cz.muni.fi.pa165.model.dto.PlaneDto;
import cz.muni.fi.pa165.resource.entity.Plane;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PlaneMapper{
    Plane toEntity(PlaneDto planeDto);
    PlaneDto toDto(Plane plane);
}
