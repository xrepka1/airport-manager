package cz.muni.fi.pa165.resource.service;

import cz.muni.fi.pa165.resource.entity.Plane;
import cz.muni.fi.pa165.resource.entity.Staff;
import cz.muni.fi.pa165.resource.repository.PlaneRepository;
import cz.muni.fi.pa165.resource.repository.StaffRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.NoSuchElementException;


@Service
public class ResourceService {

    private final PlaneRepository planeRepository;
    private final StaffRepository staffRepository;
    @Autowired
    public ResourceService(StaffRepository staffRepository, PlaneRepository planeRepository) {
        this.staffRepository = staffRepository;
        this.planeRepository = planeRepository;
    }

    /**
     *       PLANES
     **/

    @Transactional(readOnly = true)
    public Page<Plane> getAllPlanes(Pageable pageable) {
        return planeRepository.findAll(pageable);
    }

    @Transactional(readOnly = true)
    public Plane getPlaneById(Long id) {
        return planeRepository.findById(id).orElseThrow(() -> new NoSuchElementException("Plane not in database"));
    }

    public Plane createPlane(Plane plane) {
        return planeRepository.save(plane);
    }

    public void removePlane(Long id) {
        planeRepository.deleteById(id);
    }

    public Plane updatePlane(Plane plane) {
        if (planeRepository.findById(plane.getId()).isPresent()){
            return planeRepository.save(plane);
        } else {
            throw new NoSuchElementException("Plane not in database");
        }
    }


    /**
     *       STAFF
     **/

    @Transactional(readOnly = true)
    public Page<Staff> getAllStaff(Pageable pageable) {
        return staffRepository.findAll(pageable);
    }

    @Transactional(readOnly = true)
    public Staff getStaffById(Long id) {
        return staffRepository.findById(id).orElseThrow(() -> new NoSuchElementException("Staff not in database"));
    }

    public Staff createStaff(Staff staff) {
        return staffRepository.save(staff);
    }

    public void removeStaff(Long id) {
        staffRepository.deleteById(id);
    }

    public Staff updateStaff(Staff staff) {
        if (staffRepository.findById(staff.getId()).isPresent()){
            return staffRepository.save(staff);
        } else {
            throw new NoSuchElementException("Staff not in database");
        }
    }
}
