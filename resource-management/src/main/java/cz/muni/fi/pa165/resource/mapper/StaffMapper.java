package cz.muni.fi.pa165.resource.mapper;

import cz.muni.fi.pa165.model.dto.StaffDto;
import cz.muni.fi.pa165.resource.entity.Staff;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface StaffMapper {

    @Mapping(source = "position", target = "staffType")
    Staff toEntity(StaffDto staffDto);

    @Mapping(source = "staffType", target = "position")
    StaffDto toDto(Staff staff);
}
