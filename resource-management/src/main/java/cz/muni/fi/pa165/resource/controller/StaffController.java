package cz.muni.fi.pa165.resource.controller;


import cz.muni.fi.pa165.model.dto.StaffDto;
import cz.muni.fi.pa165.resource.entity.Staff;
import cz.muni.fi.pa165.resource.mapper.StaffMapper;
import cz.muni.fi.pa165.resource.service.ResourceService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;


@RestController
@RequestMapping("/api/resource-management/staff")
public class StaffController {

    private final ResourceService resourceService;
    private final StaffMapper staffMapper;

    @Autowired
    public StaffController(ResourceService resourceService, StaffMapper mapper) {
        this.resourceService = resourceService;
        this.staffMapper = mapper;
    }

    @Operation(summary = "Get all staff", description = "Gets all staff from the system.")
    @GetMapping//({""})
    public ResponseEntity<?> getAllStaff(Pageable pageable){
        try {
            Page<Staff> allStaff = resourceService.getAllStaff(pageable);
            return getListStaffEntity(allStaff);
        } catch (Exception exception) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Failed to list all staff: "+exception.getMessage());
        }
    }

    @Operation(summary = "Get staff by id", description = "Gets a staff by id from the system.")
    @GetMapping({"/id/{id}"})
    @Parameter(description = "Id of the staff")
    public ResponseEntity<?> getStaffById(@PathVariable("id") Long staffId){
        try {
            Staff staff = resourceService.getStaffById(staffId);
            return ResponseEntity.ok(staffMapper.toDto(staff));
        } catch (Exception exception) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Failed to get staff: "+exception.getMessage());
        }
    }


    @Operation(summary = "Create a staff member", description = "Creates a staff member from the request body. Ignores Id")
    @PostMapping
    public ResponseEntity<StaffDto> createStaff(@Valid @RequestBody @Parameter(description = "Staff details") StaffDto staff) {
        return ResponseEntity.ok(staffMapper.toDto(resourceService.createStaff(staffMapper.toEntity(staff))));
    }

    @Operation(summary = "Update a staff member", description = "Creates a staff member from the request body. Uses Id")
    @PutMapping
    public ResponseEntity<StaffDto> updateStaff(@Valid @RequestBody @Parameter(description = "Staff details") StaffDto staff) {
        try {
            return ResponseEntity.ok(staffMapper.toDto(resourceService.updateStaff(staffMapper.toEntity(staff))));
        } catch (NoSuchElementException e) {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body(staff);
        }
    }

    @Operation(summary = "Delete plane by Id", description = "Deletes a plane by id if it exists in the system.")
    @DeleteMapping({"/{id}"})
    public ResponseEntity<String> removeStaff(@Valid @PathVariable("id") @Parameter(description = "Staff id") Long id) {
        resourceService.removeStaff(id);
        return ResponseEntity.ok("Removed the staff member with Id " + id + " if it existed");
    }

    private ResponseEntity<List<StaffDto>> getListStaffEntity(Page<Staff> staffList) {
        if (staffList.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        List<StaffDto> result = new ArrayList<>();
        for (var staff : staffList) {
            result.add(staffMapper.toDto(staff));
        }
        return ResponseEntity.ok(result);
    }
}
