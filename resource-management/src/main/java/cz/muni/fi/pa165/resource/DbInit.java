package cz.muni.fi.pa165.resource;

import cz.muni.fi.pa165.Parser;
import cz.muni.fi.pa165.model.types.PlaneType;
import cz.muni.fi.pa165.model.types.StaffPosition;
import cz.muni.fi.pa165.resource.entity.Plane;
import cz.muni.fi.pa165.resource.entity.Staff;
import cz.muni.fi.pa165.resource.repository.PlaneRepository;
import cz.muni.fi.pa165.resource.repository.StaffRepository;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@Component
public class DbInit {

    @Autowired
    private PlaneRepository planeRepository;

    @Autowired
    private StaffRepository staffRepository;

    @PostConstruct
    private void postConstruct() throws IOException {
        List<Map<String, String>> planeEntities = Parser.parseFromCSV(
                "./src/main/java/cz/muni/fi/pa165/resource/data/plane.csv"
        );

        List<Map<String, String>> staffEntities = Parser.parseFromCSV(
                "./src/main/java/cz/muni/fi/pa165/resource/data/staff.csv"
        );


        for (Map<String, String> rawEntity : planeEntities) {
            Plane plane = new Plane();
            plane.setName(rawEntity.get("name"));
            plane.setCapacity(Integer.valueOf(rawEntity.get("capacity")));
            plane.setAge(Integer.valueOf(rawEntity.get("age")));
            plane.setPlaneType(PlaneType.valueOf(rawEntity.get("planeType")));

            planeRepository.save(plane);
        }

        for (Map<String, String> rawEntity : staffEntities) {
            Staff staff = new Staff();
            staff.setFirstName(rawEntity.get("firstName"));
            staff.setLastName(rawEntity.get("lastName"));
            staff.setStaffType(StaffPosition.valueOf(rawEntity.get("staffType")));

            staffRepository.save(staff);
        }
    }
}