package cz.muni.fi.pa165.resource;

import cz.muni.fi.pa165.model.dto.FlightDto;
import cz.muni.fi.pa165.model.dto.PlaneDto;
import cz.muni.fi.pa165.model.types.PlaneType;
import cz.muni.fi.pa165.resource.controller.PlaneController;
import cz.muni.fi.pa165.resource.entity.Plane;
import cz.muni.fi.pa165.resource.mapper.PlaneMapper;
import cz.muni.fi.pa165.resource.service.ResourceService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class PlaneControllerTests {
    @InjectMocks
    private PlaneController planeController;

    @Mock
    private ResourceService service;
    @Mock
    private PlaneMapper mapper;
    private Plane plane1;
    private PlaneDto plane1Dto;
    private Plane plane2;
    private PlaneDto plane2Dto;
    private Plane failedPlane;
    private PlaneDto failedPlaneDto;

    @BeforeEach
    public void setUp() {
        plane1 = new Plane(1L, "Airbus A380", 1000, 7, PlaneType.JUMBO_JET);
        plane2 = new Plane(1L, "Boeing 747", 410, 15, PlaneType.JUMBO_JET);
        plane1Dto = new PlaneDto(1L, "Airbus A380", 1000, 7, PlaneType.JUMBO_JET);
        plane2Dto = new PlaneDto(1L, "Boeing 747", 410, 15, PlaneType.JUMBO_JET);
        failedPlane = new Plane(1L, "Boeing 777", 610, 1, PlaneType.JUMBO_JET);
        failedPlaneDto = new PlaneDto(1L, "Boeing 777", 610, 1, PlaneType.JUMBO_JET);

        Pageable pageable = PageRequest.of(0, 2);
        Page<Plane> planePage = new PageImpl<>(List.of(plane1, plane2));

        lenient().when(service.createPlane(plane1)).thenReturn(plane1);
        lenient().when(service.updatePlane(plane1)).thenReturn(plane1);
        lenient().when(service.getAllPlanes(pageable)).thenReturn(planePage);
        lenient().when(service.getPlaneById(1L)).thenReturn(plane1);
        lenient().when(mapper.toDto(plane1)).thenReturn(plane1Dto);
        lenient().when(mapper.toDto(plane2)).thenReturn(plane2Dto);
        lenient().when(mapper.toEntity(plane1Dto)).thenReturn(plane1);
        lenient().when(mapper.toEntity(plane2Dto)).thenReturn(plane2);
        lenient().when(mapper.toEntity(failedPlaneDto)).thenReturn(failedPlane);
        lenient().when(service.updatePlane(failedPlane)).thenThrow(new NoSuchElementException("Failed to create"));
    }

    @Test
    public void testGetAllPlanes() {
        Pageable pageable = PageRequest.of(0, 2);
        ResponseEntity<?> response = planeController.getAllPlanes(pageable);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(List.of(plane1Dto, plane2Dto), response.getBody());
    }

    @Test
    public void testGetPlaneById() {
        ResponseEntity<?> response = planeController.getPlaneById(1L);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(plane1Dto, response.getBody());
    }

    @Test
    public void testPostPlane() {
        ResponseEntity<PlaneDto> response = planeController.createPlane(plane1Dto);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(plane1Dto, response.getBody());
    }

    @Test
    public void testPutPlane() {
        ResponseEntity<PlaneDto> response = planeController.updatePlane(plane1Dto);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(plane1Dto, response.getBody());
    }

    @Test
    public void testPutPlaneNotFound() {
        ResponseEntity<PlaneDto> response = planeController.updatePlane(failedPlaneDto);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertEquals(failedPlaneDto, response.getBody());
    }

    @Test
    public void testPostPlane_remove() {
        ResponseEntity<String> response = planeController.removePlane(1L);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals("Removed the plane with Id 1 if it existed", response.getBody());
        verify(service, times(1)).removePlane(1L);
    }
}

