package cz.muni.fi.pa165.resource;

import cz.muni.fi.pa165.model.dto.StaffDto;
import cz.muni.fi.pa165.model.types.StaffPosition;
import cz.muni.fi.pa165.resource.entity.Staff;
import cz.muni.fi.pa165.resource.mapper.StaffMapper;
import cz.muni.fi.pa165.resource.mapper.StaffMapperImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class StaffMapperTests {

    @InjectMocks
    private StaffMapper mapper = new StaffMapperImpl();
    private Staff staff;
    private StaffDto staffDto;

    @BeforeEach
    public void setup() {
        staff = new Staff(1L, "John", "Doe", StaffPosition.PILOT);
        staffDto = new StaffDto(1L, "John", "Doe", StaffPosition.PILOT);
    }

    @Test
    public void testToDto() {
        var resultDto = mapper.toDto(staff);

        assertEquals(staff.getId(), resultDto.getId());
        assertEquals(staff.getFirstName(), resultDto.getFirstName());
        assertEquals(staff.getLastName(), resultDto.getLastName());
        assertEquals(staff.getStaffType(), resultDto.getPosition());
    }

    @Test
    public void testToEntity() {
        var resultEntity = mapper.toEntity(staffDto);

        assertEquals(staffDto.getId(), resultEntity.getId());
        assertEquals(staffDto.getFirstName(), resultEntity.getFirstName());
        assertEquals(staffDto.getLastName(), resultEntity.getLastName());
        assertEquals(staffDto.getPosition(), resultEntity.getStaffType());
    }
}
