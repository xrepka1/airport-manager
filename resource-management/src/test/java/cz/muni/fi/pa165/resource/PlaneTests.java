package cz.muni.fi.pa165.resource;

import cz.muni.fi.pa165.model.types.PlaneType;
import cz.muni.fi.pa165.resource.entity.Plane;
import cz.muni.fi.pa165.resource.repository.PlaneRepository;
import cz.muni.fi.pa165.resource.repository.StaffRepository;
import cz.muni.fi.pa165.resource.service.ResourceService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
@DataJpaTest
public class PlaneTests {

    @Mock
    private PlaneRepository planeRepository;

    @Mock
    private StaffRepository staffRepository;

    @InjectMocks
    private ResourceService service;

    private Plane plane;

    @BeforeEach
    public void setup() {
        service = new ResourceService(staffRepository, planeRepository);
        plane = new Plane(
                1L,
                "Boeing 747",
                410,
                15,
                PlaneType.JUMBO_JET
        );
    }

    @Test
    public void testCreatePlane() {
        when(planeRepository.save(any(Plane.class))).thenReturn(plane);

        Plane createdPlane = service.createPlane(
                new Plane(
                        1L,
                        "Boeing 747",
                        410,
                        15,
                        PlaneType.JUMBO_JET
                )
        );
        assertNotNull(createdPlane);
        assertEquals(plane.getName(), createdPlane.getName());
        assertEquals(plane.getCapacity(), createdPlane.getCapacity());
        assertEquals(plane.getAge(), createdPlane.getAge());
        assertEquals(plane.getPlaneType(), createdPlane.getPlaneType());
    }

    @Test
    public void testGetAllPlanes() {
        Pageable pageable = PageRequest.of(0, 2);
        Page<Plane> planePage = new PageImpl<>(List.of(plane));

        when(planeRepository.findAll(any(Pageable.class))).thenReturn(planePage);

        Page<Plane> planes = service.getAllPlanes(pageable);
        assertNotNull(planes);
        assertFalse(planes.isEmpty());
        assertEquals(1, planes.getTotalElements());
        assertEquals(plane, planes.getContent().get(0));
    }

    @Test
    public void testUpdateDestination() {
        Plane updatedPlane = new Plane(
                1L,
                "Airbus A380",
                1000,
                7,
                PlaneType.JUMBO_JET
        );

        when(planeRepository.findById(1L)).thenReturn(Optional.of(plane));
        when(planeRepository.save(any(Plane.class))).thenReturn(updatedPlane);

        Plane result = service.updatePlane(updatedPlane);
        assertNotNull(result);
        assertEquals(result.getName(), updatedPlane.getName());
        assertEquals(result.getCapacity(), updatedPlane.getCapacity());
        assertEquals(result.getAge(), updatedPlane.getAge());
        assertEquals(result.getPlaneType(), updatedPlane.getPlaneType());
    }

    @Test
    public void testGetPlaneById() {
        when(planeRepository.findById(1L)).thenReturn(Optional.of(plane));

        Plane result = service.getPlaneById(1L);
        assertNotNull(result);
        assertEquals(plane, result);
    }

    @Test
    public void testRemovePlane() {
        doNothing().when(planeRepository).deleteById(plane.getId());
        service.removePlane(plane.getId());
        verify(planeRepository, times(1)).deleteById(plane.getId());

    }
}
