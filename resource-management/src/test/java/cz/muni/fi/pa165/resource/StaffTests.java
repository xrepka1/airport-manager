package cz.muni.fi.pa165.resource;

import cz.muni.fi.pa165.model.types.StaffPosition;
import cz.muni.fi.pa165.resource.entity.Plane;
import cz.muni.fi.pa165.resource.entity.Staff;
import cz.muni.fi.pa165.resource.repository.PlaneRepository;
import cz.muni.fi.pa165.resource.repository.StaffRepository;
import cz.muni.fi.pa165.resource.service.ResourceService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
@DataJpaTest
public class StaffTests {

    @Mock
    private PlaneRepository planeRepository;

    @Mock
    private StaffRepository staffRepository;

    @InjectMocks
    private ResourceService service;

    private Staff staff;

    @BeforeEach
    public void setup() {
        service = new ResourceService(staffRepository, planeRepository);
        staff = new Staff(
                1L,
                "John",
                "Doe",
                StaffPosition.PILOT
        );
    }

    @Test
    public void testCreateStaff() {
        when(staffRepository.save(any(Staff.class))).thenReturn(staff);

        Staff createdStaff = service.createStaff(
                new Staff(
                        1L,
                        "John",
                        "Doe",
                        StaffPosition.PILOT
                )
        );
        assertNotNull(createdStaff);
        assertEquals(staff.getFirstName(), createdStaff.getFirstName());
        assertEquals(staff.getLastName(), createdStaff.getLastName());
        assertEquals(staff.getStaffType(), createdStaff.getStaffType());
    }

    @Test
    public void testGetAllStaffs() {
        Pageable pageable = PageRequest.of(0, 2);
        Page<Staff> staffPage = new PageImpl<>(List.of(staff));

        when(staffRepository.findAll(any(Pageable.class))).thenReturn(staffPage);

        Page<Staff> staffList = service.getAllStaff(pageable);
        assertNotNull(staffList);
        assertFalse(staffList.isEmpty());
        assertEquals(1, staffList.getTotalElements());
        assertEquals(staff, staffList.getContent().get(0));
    }

    @Test
    public void testUpdateDestination() {
        Staff updatedStaff = new Staff(
                1L,
                "John",
                "Doe",
                StaffPosition.PILOT
        );

        when(staffRepository.findById(1L)).thenReturn(Optional.of(staff));
        when(staffRepository.save(any(Staff.class))).thenReturn(updatedStaff);

        Staff result = service.updateStaff(updatedStaff);
        assertNotNull(result);
        assertEquals(staff.getFirstName(), result.getFirstName());
        assertEquals(staff.getLastName(), result.getLastName());
        assertEquals(staff.getStaffType(), result.getStaffType());
    }

    @Test
    public void testGetStaffById() {
        when(staffRepository.findById(1L)).thenReturn(Optional.of(staff));

        Staff result = service.getStaffById(1L);
        assertNotNull(result);
        assertEquals(staff, result);
    }

    @Test
    public void testRemoveStaff() {
        doNothing().when(staffRepository).deleteById(staff.getId());
        service.removeStaff(staff.getId());
        verify(staffRepository, times(1)).deleteById(staff.getId());

    }
}
