package cz.muni.fi.pa165.resource;

import cz.muni.fi.pa165.model.dto.StaffDto;
import cz.muni.fi.pa165.model.types.StaffPosition;
import cz.muni.fi.pa165.resource.controller.StaffController;
import cz.muni.fi.pa165.resource.entity.Plane;
import cz.muni.fi.pa165.resource.entity.Staff;
import cz.muni.fi.pa165.resource.mapper.StaffMapper;
import cz.muni.fi.pa165.resource.service.ResourceService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class StaffControllerTests {
    @InjectMocks
    private StaffController staffController;

    @Mock
    private ResourceService service;
    @Mock
    private StaffMapper mapper;
    private Staff staff1;
    private StaffDto staff1Dto;
    private Staff staff2;
    private StaffDto staff2Dto;
    private Staff failedStaff;
    private StaffDto failedStaffDto;

    @BeforeEach
    public void setUp() {
        staff1 = new Staff(1L, "John", "Doe", StaffPosition.PILOT);
        staff2 = new Staff(2L, "Joanna", "Doe", StaffPosition.STEWARD);
        staff1Dto = new StaffDto(1L, "John", "Doe", StaffPosition.PILOT);
        staff2Dto = new StaffDto(2L, "Joanna", "Doe", StaffPosition.STEWARD);
        failedStaff = new Staff(2L, "Harry", "Potter", StaffPosition.PILOT);
        failedStaffDto = new StaffDto(2L, "Harry", "Potter", StaffPosition.PILOT);

        Pageable pageable = PageRequest.of(0, 2);
        Page<Staff> staffPage = new PageImpl<>(List.of(staff1, staff2));

        lenient().when(service.createStaff(staff1)).thenReturn(staff1);
        lenient().when(service.updateStaff(staff1)).thenReturn(staff1);
        lenient().when(service.getAllStaff(pageable)).thenReturn(staffPage);
        lenient().when(service.getStaffById(1L)).thenReturn(staff1);
        lenient().when(mapper.toDto(staff1)).thenReturn(staff1Dto);
        lenient().when(mapper.toDto(staff2)).thenReturn(staff2Dto);
        lenient().when(mapper.toEntity(staff1Dto)).thenReturn(staff1);
        lenient().when(mapper.toEntity(staff2Dto)).thenReturn(staff2);
        lenient().when(mapper.toEntity(failedStaffDto)).thenReturn(failedStaff);
        lenient().when(service.updateStaff(failedStaff)).thenThrow(new NoSuchElementException("Text"));
    }

    @Test
    public void testGetAllStaffs() {
        Pageable pageable = PageRequest.of(0, 2);
        ResponseEntity<?> response = staffController.getAllStaff(pageable);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        List<StaffDto> responseBody = (List<StaffDto>) response.getBody();
        assertEquals(List.of(staff1Dto, staff2Dto), responseBody);
    }

    @Test
    public void testGetStaffById() {
        ResponseEntity<?> response = staffController.getStaffById(1L);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(staff1Dto, response.getBody());
    }

    @Test
    public void testPostStaff() {
        ResponseEntity<StaffDto> response = staffController.createStaff(staff1Dto);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(staff1Dto, response.getBody());
    }

    @Test
    public void testPutStaff() {
        ResponseEntity<StaffDto> response = staffController.updateStaff(staff1Dto);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(staff1Dto, response.getBody());
    }

    @Test
    public void testPutStaffNotFound() {
        ResponseEntity<StaffDto> response = staffController.updateStaff(failedStaffDto);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertEquals(failedStaffDto, response.getBody());
    }

    @Test
    public void testPostStaff_remove() {
        ResponseEntity<String> response = staffController.removeStaff(1L);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals("Removed the staff member with Id 1 if it existed", response.getBody());
        verify(service, times(1)).removeStaff(1L);
    }
}

