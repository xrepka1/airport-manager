package cz.muni.fi.pa165.resource;

import cz.muni.fi.pa165.model.dto.PlaneDto;
import cz.muni.fi.pa165.model.types.PlaneType;
import cz.muni.fi.pa165.resource.entity.Plane;
import cz.muni.fi.pa165.resource.mapper.PlaneMapper;
import cz.muni.fi.pa165.resource.mapper.PlaneMapperImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class PlaneMapperTests {

    @InjectMocks
    private PlaneMapper mapper = new PlaneMapperImpl();
    private Plane plane;
    private PlaneDto planeDto;

    @BeforeEach
    public void setup() {
        plane = new Plane(1L, "Airbus A380", 1000, 7, PlaneType.JUMBO_JET);
        planeDto = new PlaneDto(1L, "Airbus A380", 1000, 7, PlaneType.JUMBO_JET);
    }

    @Test
    public void testToDto() {
        var resultDto = mapper.toDto(plane);

        assertEquals(plane.getId(), resultDto.getId());
        assertEquals(plane.getAge(), resultDto.getAge());
        assertEquals(plane.getName(), resultDto.getName());
        assertEquals(plane.getCapacity(), resultDto.getCapacity());
        assertEquals(plane.getPlaneType(), resultDto.getPlaneType());
    }

    @Test
    public void testToEntity() {
        var resultEntity = mapper.toEntity(planeDto);

        assertEquals(planeDto.getId(), resultEntity.getId());
        assertEquals(planeDto.getAge(), resultEntity.getAge());
        assertEquals(planeDto.getName(), resultEntity.getName());
        assertEquals(planeDto.getCapacity(), resultEntity.getCapacity());
        assertEquals(planeDto.getPlaneType(), resultEntity.getPlaneType());
    }
}
