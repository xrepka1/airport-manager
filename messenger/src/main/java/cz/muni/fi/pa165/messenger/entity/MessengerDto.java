package cz.muni.fi.pa165.messenger.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Objects;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class MessengerDto {
    private String toAddress;
    private String subject;
    private String body;
    private Long id;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MessengerDto that = (MessengerDto) o;
        return Objects.equals(toAddress, that.toAddress) && Objects.equals(subject, that.subject) && Objects.equals(body, that.body) && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(toAddress, subject, body, id);
    }
}
