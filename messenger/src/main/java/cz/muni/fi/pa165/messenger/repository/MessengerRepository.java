package cz.muni.fi.pa165.messenger.repository;

import cz.muni.fi.pa165.messenger.entity.Messenger;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

@org.springframework.stereotype.Repository
public interface MessengerRepository extends JpaRepository<Messenger, Long> {
    @Query("SELECT m FROM Messenger m WHERE m.subject LIKE %:keyword%")
    List<Messenger> findBySubjectContaining(@Param("keyword") String keyword);
    @Query("SELECT m FROM Messenger m WHERE m.toAddress = :toAddress")
    List<Messenger> findByToAddress(@Param("toAddress") String toAddress);

}
