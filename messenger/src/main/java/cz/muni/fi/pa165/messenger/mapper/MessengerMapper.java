package cz.muni.fi.pa165.messenger.mapper;

import cz.muni.fi.pa165.messenger.entity.Messenger;
import cz.muni.fi.pa165.messenger.entity.MessengerDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface MessengerMapper {
    Messenger toEntity(MessengerDto messengerDto);
    MessengerDto toDto(Messenger entity);
}
