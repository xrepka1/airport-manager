package cz.muni.fi.pa165.messenger.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.validation.constraints.Email;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Messenger {
    @Email
    private String toAddress;

    private String subject;

    private String body;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
}