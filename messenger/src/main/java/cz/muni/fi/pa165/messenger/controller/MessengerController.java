package cz.muni.fi.pa165.messenger.controller;

import cz.muni.fi.pa165.messenger.entity.Messenger;
import cz.muni.fi.pa165.messenger.entity.MessengerDto;
import cz.muni.fi.pa165.messenger.mapper.MessengerMapper;
import cz.muni.fi.pa165.messenger.service.MessengerService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/messages")
public class MessengerController {
    private final MessengerService messengerService;
    private final MessengerMapper messengerMapper;

    @Autowired
    public MessengerController(MessengerService messengerService, MessengerMapper messengerMapper) {
        this.messengerService = messengerService;
        this.messengerMapper = messengerMapper;
    }


    @Operation(summary = "Sends a message", description = "Sends a message with the specified parameters.")
    @PostMapping("/send")
    public ResponseEntity<String> sendMessage(@RequestBody Messenger messenger){
        try {
            messengerService.sendMessage(messenger);
            return ResponseEntity.ok("Message sent successfully");
        } catch (Exception exception) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Failed to send message: "+exception.getMessage());
        }
    }

    @GetMapping("/to-address/")
    public ResponseEntity<List<MessengerDto>> getMessagesByToAddress(@RequestParam String address) {
        List<Messenger> messages = messengerService.getMessagesByToAddress(address);
        return getListResponseEntity(messages);
    }

    @GetMapping("/subject/")
    public ResponseEntity<List<MessengerDto>> getMessagesBySubjectContaining(@RequestParam String keyword) {
        List<Messenger> messages = messengerService.getMessagesBySubjectContaining(keyword);
        return getListResponseEntity(messages);
    }

    private ResponseEntity<List<MessengerDto>> getListResponseEntity(List<Messenger> messages) {
        if (messages.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        List<MessengerDto> result = new ArrayList<>();
        for (var message : messages) {
            result.add(messengerMapper.toDto(message));
        }
        return ResponseEntity.ok(result);
    }

}
