package cz.muni.fi.pa165.messenger.service;


import cz.muni.fi.pa165.messenger.entity.Messenger;
import cz.muni.fi.pa165.messenger.repository.MessengerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.regex.Pattern;

@Service
public class MessengerService {
    private JavaMailSender javaMailSender;
    private final MessengerRepository messengerRepository;

    private static final Pattern EMAIL_PATTERN = Pattern.compile(
            "^(?=.{1,64}@)[A-Za-z0-9_-]+(\\.[A-Za-z0-9_-]+)*@[^-][A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$"
    );

    @Autowired
    public MessengerService(MessengerRepository messengerRepository,JavaMailSender javaMailSender) {
        this.messengerRepository = messengerRepository;
        this.javaMailSender = javaMailSender;
    }

    public void sendMessage (Messenger messenger)  {
        validateEmail(messenger.getToAddress());
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(messenger.getToAddress());
        message.setSubject(messenger.getSubject());
        message.setText(messenger.getBody());
        //needs username and password
        //javaMailSender.send(message);
        save(messenger);
    }
    private void validateEmail(String address) {
        if (address == null || !EMAIL_PATTERN.matcher(address).matches()) {
            throw new IllegalArgumentException("Invalid email address: " + address);
        }
    }
    public void save(Messenger messenger) {
        messengerRepository.save(messenger);
    }

    public List<Messenger> getMessagesByToAddress(String toAddress){
        return messengerRepository.findByToAddress(toAddress);
    }
    public List<Messenger> getMessagesBySubjectContaining(String keyword){
        return messengerRepository.findBySubjectContaining(keyword);
    }


}
