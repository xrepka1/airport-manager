package cz.muni.fi.pa165.messenger;

import cz.muni.fi.pa165.messenger.entity.Messenger;
import cz.muni.fi.pa165.messenger.repository.MessengerRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class MessengerRepositoryTests {
    @Mock
    private MessengerRepository repository;

    @InjectMocks
    private Messenger messenger;
    @InjectMocks
    private Messenger messenger2;
    @InjectMocks
    private Messenger messenger3;

    @BeforeEach
    public void setup() {
        messenger = new Messenger();
        messenger.setToAddress("mail@domain.com");
        messenger.setSubject("Test subject");
        messenger.setBody("Messenger Test body text");
        messenger2 = new Messenger();
        messenger2.setToAddress("mail2@anotherdomain@org");
        messenger2.setSubject("Testing subject");
        messenger2.setBody("Another messenger body text");
        messenger3 = new Messenger();
        messenger3.setToAddress("mail2@anotherdomain@org");
    }

    @Test
    public void testGetMessagesByToAddress() {
        when(repository.findByToAddress(any(String.class))).thenReturn(List.of(messenger));

        List<Messenger> addressMessages = repository.findByToAddress(messenger.getToAddress());
        assertNotNull(addressMessages);
        assertEquals(addressMessages.size(), 1);
        assertEquals(addressMessages.get(0).getToAddress(), messenger.getToAddress());
        assertEquals(addressMessages.get(0).getSubject(), messenger.getSubject());
        assertEquals(addressMessages.get(0).getBody(), messenger.getBody());
    }

    @Test
    public void testGetMessagesByToAddress2() {
        when(repository.findByToAddress(any(String.class))).thenReturn(List.of(messenger2, messenger3));

        List<Messenger> addressMessages = repository.findByToAddress(messenger2.getToAddress());
        assertEquals(addressMessages.size(), 2);
        assertEquals(addressMessages.get(0), messenger2);
        assertEquals(addressMessages.get(1), messenger3);
    }

    @Test
    public void testFindBySubjectContaining() {
        when(repository.findBySubjectContaining(any(String.class))).thenReturn(List.of(messenger));

        List<Messenger> addressMessages = repository.findBySubjectContaining(messenger.getSubject().substring(2));
        assertNotNull(addressMessages);
        assertEquals(addressMessages.size(), 1);
        assertEquals(addressMessages.get(0).getToAddress(), messenger.getToAddress());
        assertEquals(addressMessages.get(0).getSubject(), messenger.getSubject());
        assertEquals(addressMessages.get(0).getBody(), messenger.getBody());
    }

    @Test
    public void testFindBySubjectContaining2() {
        when(repository.findBySubjectContaining(any(String.class))).thenReturn(List.of(messenger, messenger2));

        List<Messenger> addressMessages = repository.findBySubjectContaining(messenger.getSubject().substring(4));
        assertNotNull(addressMessages);
        assertEquals(addressMessages.size(), 2);
        assertEquals(addressMessages.get(0), messenger);
        assertEquals(addressMessages.get(1), messenger2);
    }

}
