package cz.muni.fi.pa165.messenger;

import cz.muni.fi.pa165.messenger.controller.MessengerController;
import cz.muni.fi.pa165.messenger.entity.Messenger;
import cz.muni.fi.pa165.messenger.entity.MessengerDto;
import cz.muni.fi.pa165.messenger.mapper.MessengerMapper;
import cz.muni.fi.pa165.messenger.service.MessengerService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class MessengerControllerTests {

    @Mock
    private MessengerService service;

    @Mock
    private MessengerMapper mapper;

    @InjectMocks
    private MessengerController controller;

    private Messenger messenger;
    private Messenger messenger2;
    private Messenger messenger3;
    private MessengerDto messengerDto;
    private MessengerDto messengerDto2;
    private MessengerDto messengerDto3;

    @BeforeEach
    public void setup() {
        controller = new MessengerController(service, mapper);
        messenger = new Messenger();
        messenger.setToAddress("mail@domain.com");
        messenger.setSubject("Test subject");
        messenger.setBody("Messenger Test body text");
        messenger2 = new Messenger();
        messenger2.setToAddress("mail2@anotherdomain@org");
        messenger2.setSubject("Testing subject");
        messenger2.setBody("Another messenger body text");
        messenger3 = new Messenger();
        messenger3.setToAddress("mail2@anotherdomain@org");
        messengerDto = new MessengerDto();
        messengerDto.setToAddress("mail@domain.com");
        messengerDto.setSubject("Test subject");
        messengerDto.setBody("Messenger Test body text");
        messengerDto2 = new MessengerDto();
        messengerDto2.setToAddress("mail2@anotherdomain@org");
        messengerDto2.setSubject("Testing subject");
        messengerDto2.setBody("Another messenger body text");
        messengerDto3 = new MessengerDto();
        messengerDto3.setToAddress("mail2@anotherdomain@org");
    }

    @Test
    public void testGetMessagesByToAddress() {
        when(service.getMessagesByToAddress(any(String.class))).thenReturn(List.of(messenger));
        when(mapper.toDto(messenger)).thenReturn(messengerDto);

        ResponseEntity<List<MessengerDto>> response = controller.getMessagesByToAddress(messenger.getToAddress());
        assertNotNull(response);
        assertEquals(response.getStatusCode().value(), 200);
        assertTrue(response.hasBody());
        List<MessengerDto> addressMessages = response.getBody();
        assertNotNull(addressMessages);
        assertEquals(addressMessages.size(), 1);
        assertEquals(addressMessages.get(0).getToAddress(), messenger.getToAddress());
        assertEquals(addressMessages.get(0).getSubject(), messenger.getSubject());
        assertEquals(addressMessages.get(0).getBody(), messenger.getBody());
    }

    @Test
    public void testGetMessagesByToAddress2() {
        when(service.getMessagesByToAddress(any(String.class))).thenReturn(List.of(messenger2, messenger3));
        when(mapper.toDto(messenger2)).thenReturn(messengerDto2);
        when(mapper.toDto(messenger3)).thenReturn(messengerDto3);

        ResponseEntity<List<MessengerDto>> response = controller.getMessagesByToAddress(messenger2.getToAddress());
        assertNotNull(response);
        assertEquals(response.getStatusCode().value(), 200);
        assertTrue(response.hasBody());
        List<MessengerDto> addressMessages = response.getBody();
        assertNotNull(addressMessages);
        assertEquals(addressMessages.size(), 2);
        assertEquals(addressMessages.get(0), messengerDto2);
        assertEquals(addressMessages.get(1), messengerDto3);
    }

    @Test
    public void testGetMessagesBySubjectContaining() {
        when(service.getMessagesBySubjectContaining(any(String.class))).thenReturn(List.of(messenger));
        when(mapper.toDto(messenger)).thenReturn(messengerDto);

        ResponseEntity<List<MessengerDto>> response = controller.getMessagesBySubjectContaining(messenger.getSubject().substring(2));
        assertNotNull(response);
        assertEquals(response.getStatusCode().value(), 200);
        assertTrue(response.hasBody());
        List<MessengerDto> addressMessages = response.getBody();
        assertNotNull(addressMessages);
        assertEquals(addressMessages.size(), 1);
        assertEquals(addressMessages.get(0).getToAddress(), messenger.getToAddress());
        assertEquals(addressMessages.get(0).getSubject(), messenger.getSubject());
        assertEquals(addressMessages.get(0).getBody(), messenger.getBody());
    }

    @Test
    public void testGetMessagesBySubjectContaining2() {
        when(service.getMessagesBySubjectContaining(any(String.class))).thenReturn(List.of(messenger, messenger2));
        when(mapper.toDto(messenger)).thenReturn(messengerDto);
        when(mapper.toDto(messenger2)).thenReturn(messengerDto2);

        ResponseEntity<List<MessengerDto>> response = controller.getMessagesBySubjectContaining(messenger.getSubject().substring(5));
        assertNotNull(response);
        assertEquals(response.getStatusCode().value(), 200);
        assertTrue(response.hasBody());
        List<MessengerDto> addressMessages = response.getBody();
        assertNotNull(addressMessages);
        assertEquals(addressMessages.size(), 2);
        assertEquals(addressMessages.get(0), messengerDto);
        assertEquals(addressMessages.get(1), messengerDto2);
    }

    @Test
    public void testSendMessage() {
        doNothing().when(service).sendMessage(any(Messenger.class));

        ResponseEntity<String> response = controller.sendMessage(messenger);
        assertNotNull(response);
        assertEquals(response.getStatusCode().value(), 200);
        assertTrue(response.hasBody());
        String responseMessage = response.getBody();
        assertNotNull(responseMessage);
        assertEquals(responseMessage, "Message sent successfully");
    }

    @Test
    public void testSendMessage2() {
        //doThrow(new MailcapParseException("Invalid test subject")).when(service).sendMessage(any(Messenger.class));

        Mockito.doThrow(new RuntimeException("Invalid test subject")).when(service).sendMessage(messenger3);
        ResponseEntity<String> response = controller.sendMessage(messenger3);
        assertNotNull(response);
        assertEquals(response.getStatusCode().value(), 500);
        assertTrue(response.hasBody());
        String responseMessage = response.getBody();
        assertNotNull(responseMessage);
        assertEquals(responseMessage, "Failed to send message: Invalid test subject");
    }
}
