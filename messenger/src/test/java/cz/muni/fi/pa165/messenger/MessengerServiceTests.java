package cz.muni.fi.pa165.messenger;

import cz.muni.fi.pa165.messenger.entity.Messenger;
import cz.muni.fi.pa165.messenger.repository.MessengerRepository;
import cz.muni.fi.pa165.messenger.service.MessengerService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.mail.javamail.JavaMailSender;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
@DataJpaTest
class MessengerServiceTests {
    @Mock
    private MessengerRepository repository;

    @Mock
    private JavaMailSender mailSender;

    @InjectMocks
    private MessengerService service;

    private Messenger messenger;
    private Messenger messenger2;
    private Messenger messenger3;

    @BeforeEach
    public void setup() {
        service = new MessengerService(repository, mailSender);
        messenger = new Messenger();
        messenger.setToAddress("mail@domain.com");
        messenger.setSubject("Test subject");
        messenger.setBody("Messenger Test body text");
        messenger2 = new Messenger();
        messenger2.setToAddress("mail2@anotherdomain@org");
        messenger2.setSubject("Testing subject");
        messenger2.setBody("Another messenger body text");
        messenger3 = new Messenger();
        messenger3.setToAddress("mail2@anotherdomain@org");
    }

    @Test
    public void testGetMessagesByToAddress() {
        when(repository.findByToAddress(any(String.class))).thenReturn(List.of(messenger));

        List<Messenger> addressMessages = service.getMessagesByToAddress(messenger.getToAddress());
        assertNotNull(addressMessages);
        assertEquals(addressMessages.size(), 1);
        assertEquals(addressMessages.get(0).getToAddress(), messenger.getToAddress());
        assertEquals(addressMessages.get(0).getSubject(), messenger.getSubject());
        assertEquals(addressMessages.get(0).getBody(), messenger.getBody());
    }

    @Test
    public void testGetMessagesByToAddress2() {
        when(repository.findByToAddress(any(String.class))).thenReturn(List.of(messenger2, messenger3));

        List<Messenger> addressMessages = service.getMessagesByToAddress(messenger2.getToAddress());
        assertEquals(addressMessages.size(), 2);
        assertEquals(addressMessages.get(0), messenger2);
        assertEquals(addressMessages.get(1), messenger3);
    }

    @Test
    public void testGetMessagesBySubjectContaining() {
        when(repository.findBySubjectContaining(any(String.class))).thenReturn(List.of(messenger));

        List<Messenger> addressMessages = service.getMessagesBySubjectContaining(messenger.getSubject().substring(2));
        assertNotNull(addressMessages);
        assertEquals(addressMessages.size(), 1);
        assertEquals(addressMessages.get(0).getToAddress(), messenger.getToAddress());
        assertEquals(addressMessages.get(0).getSubject(), messenger.getSubject());
        assertEquals(addressMessages.get(0).getBody(), messenger.getBody());
    }

    @Test
    public void testGetMessagesBySubjectContaining2() {
        when(repository.findBySubjectContaining(any(String.class))).thenReturn(List.of(messenger, messenger2));

        List<Messenger> addressMessages = service.getMessagesBySubjectContaining(messenger.getSubject().substring(4));
        assertNotNull(addressMessages);
        assertEquals(addressMessages.size(), 2);
        assertEquals(addressMessages.get(0), messenger);
        assertEquals(addressMessages.get(1), messenger2);
    }
}

