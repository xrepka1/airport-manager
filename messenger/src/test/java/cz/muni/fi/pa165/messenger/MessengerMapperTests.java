package cz.muni.fi.pa165.messenger;

import cz.muni.fi.pa165.messenger.entity.Messenger;
import cz.muni.fi.pa165.messenger.entity.MessengerDto;
import cz.muni.fi.pa165.messenger.mapper.MessengerMapper;
import cz.muni.fi.pa165.messenger.mapper.MessengerMapperImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class MessengerMapperTests {

    @InjectMocks
    private MessengerMapper mapper = new MessengerMapperImpl();
    private Messenger messenger;
    private MessengerDto messengerDto;

    @BeforeEach
    public void setup() {
        messenger = new Messenger();
        messenger.setToAddress("mail@domain.com");
        messenger.setSubject("Test subject");
        messenger.setBody("Messenger Test body text");
        messengerDto = new MessengerDto();
        messengerDto.setToAddress("mail@domain.com");
        messengerDto.setSubject("Test subject");
        messengerDto.setBody("Messenger Test body text");
    }

    @Test
    public void testToDto() {
        var resultDto = mapper.toDto(messenger);

        assertEquals(messenger.getId(), resultDto.getId());
        assertEquals(messenger.getToAddress(), resultDto.getToAddress());
        assertEquals(messenger.getSubject(), resultDto.getSubject());
        assertEquals(messenger.getBody(), resultDto. getBody());
    }

    @Test
    public void testToEntity() {
        var resultEntity = mapper.toEntity(messengerDto);

        assertEquals(messenger.getId(), resultEntity.getId());
        assertEquals(messenger.getToAddress(), resultEntity.getToAddress());
        assertEquals(messenger.getSubject(), resultEntity.getSubject());
        assertEquals(messenger.getBody(), resultEntity. getBody());
    }
}
