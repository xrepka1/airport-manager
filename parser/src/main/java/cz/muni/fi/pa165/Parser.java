package cz.muni.fi.pa165;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Parser {

    public static boolean importFromCSV(String filePath) {
        try {
            List<Map<String, String>> csvData = parseFromCSV(filePath);

            for (Map<String, String> record : csvData) {
                if (isDefined(record.get("name")) && isDefined(record.get("iata_code")) && isDefined(record.get("iso_country"))){
                    System.out.printf("Name: %s, Code: %s, Country: %s%n",record.get("name"), record.get("iata_code"), record.get("iso_country"));
                }
            }

            // Import data to H2 database
            // ...

        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public static List<Map<String, String>> parseFromCSV(String filePath) throws IOException {
        List<Map<String, String>> recordsList = new ArrayList<>();

        try (FileReader fileReader = new FileReader(filePath, StandardCharsets.UTF_8)) {
            CSVParser csvParser = new CSVParser(fileReader, CSVFormat.DEFAULT.builder()
                    .setHeader()
                    .setSkipHeaderRecord(true)
                    .setTrim(true)
                    .setDelimiter(',')
                    .build());


            mapRecords(recordsList, csvParser);
        }

        return recordsList;
    }

    public List<Map<String, String>> parseFromCSV(String filePath, char delimiter) throws IOException {
        List<Map<String, String>> recordsList = new ArrayList<>();

        try (FileReader fileReader = new FileReader(filePath, StandardCharsets.UTF_8)) {
            CSVParser csvParser = new CSVParser(fileReader, CSVFormat.DEFAULT.builder()
                    .setHeader()
                    .setSkipHeaderRecord(true)
                    .setTrim(true)
                    .setDelimiter(delimiter)
                    .build());


            mapRecords(recordsList, csvParser);
        }

        return recordsList;
    }

    private static void mapRecords(List<Map<String, String>> recordsList, CSVParser csvParser) {
        for (CSVRecord record : csvParser) {
            Map<String, String> recordMap = new HashMap<>();
            for (String header : csvParser.getHeaderNames()) {
                recordMap.put(header, record.get(header));
            }
            recordsList.add(recordMap);
        }
    }


    private static boolean isDefined(String value) {
        return value != null && !value.trim().isEmpty();
    }
}