package cz.muni.fi.pa165.model.types;

public enum FlightStatus {
    SCHEDULED,
    BOARDING,
    DEPARTED,
    IN_AIR,
    ARRIVED,
    CANCELLED,
    DELAYED
}
