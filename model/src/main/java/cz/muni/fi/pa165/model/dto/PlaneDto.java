package cz.muni.fi.pa165.model.dto;

import cz.muni.fi.pa165.model.common.DomainObjectDto;
import cz.muni.fi.pa165.model.types.PlaneType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public class PlaneDto extends DomainObjectDto {
    private Long id;
    private String name;
    private Integer capacity;
    private Integer age;
    private PlaneType planeType;
}

