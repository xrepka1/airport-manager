package cz.muni.fi.pa165.model.types;

public enum StaffPosition {
    PILOT,
    STEWARD
}
