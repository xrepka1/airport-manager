package cz.muni.fi.pa165.model.dto;

import cz.muni.fi.pa165.model.common.DomainObjectDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class DestinationDto extends DomainObjectDto {
    private Long id;
    private String country;
    private String city;
    private String airportCode;
}
