package cz.muni.fi.pa165.model.dto;

import cz.muni.fi.pa165.model.types.FlightStatus;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FlightDto {
    private Long id;
    private String name;
    private Long plane;
    private LocalDateTime departure;
    private LocalDateTime arrival;
    private String airportFrom;
    private String airportTo;
    private FlightStatus flightStatus;
    private List<Long> staff;
}
