package cz.muni.fi.pa165.model.common;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class DomainObjectDto {
    private Long id;
}