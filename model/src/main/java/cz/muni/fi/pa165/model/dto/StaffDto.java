package cz.muni.fi.pa165.model.dto;

import cz.muni.fi.pa165.model.types.StaffPosition;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class StaffDto {
    private Long id;
    private String firstName;
    private String lastName;
    private StaffPosition position;
}
