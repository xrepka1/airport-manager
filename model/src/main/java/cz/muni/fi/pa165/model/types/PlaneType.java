package cz.muni.fi.pa165.model.types;

public enum PlaneType{
    JUMBO_JET,
    LIGHT_JET,
    MID_SIZE_JET,
    TURBOPROP
}
