package cz.muni.fi.pa165.manager;

import cz.muni.fi.pa165.manager.controller.flights.FlightController;
import cz.muni.fi.pa165.manager.entity.Flight;
import cz.muni.fi.pa165.manager.mapper.FlightMapper;
import cz.muni.fi.pa165.manager.service.FlightManagerService;
import cz.muni.fi.pa165.model.dto.FlightDto;
import cz.muni.fi.pa165.model.types.FlightStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class FlightControllerTests {
    @InjectMocks
    private FlightController flightController;

    @Mock
    private FlightManagerService service;

    @Mock
    private FlightMapper mapper;

    private Flight flight1;
    private FlightDto flight1Dto;
    private Flight flight2;
    private FlightDto flight2Dto;

    @BeforeEach
    public void setUp() {
        flight1 = new Flight(
                1L,
                "BRQ01",
                1L,
                LocalDateTime.of(2023, 3, 26, 14, 30),
                LocalDateTime.of(2023, 3, 26, 14, 30),
                "BRQ",
                "BTA",
                FlightStatus.SCHEDULED,
                List.of(1L, 2L, 3L)
        );
        flight1.setId(1L);

        flight2 = new Flight(
                2L,
                "BTA01",
                1L,
                LocalDateTime.of(2023, 3, 26, 14, 30),
                LocalDateTime.of(2023, 3, 26, 14, 30),
                "BTA",
                "BRQ",
                FlightStatus.SCHEDULED,
                List.of(4L, 5L, 6L)
        );
        flight2.setId(2L);
        flight1Dto = new FlightDto(
                1L,
                "BRQ01",
                1L,
                LocalDateTime.of(2023, 3, 26, 14, 30),
                LocalDateTime.of(2023, 3, 26, 14, 30),
                "BRQ",
                "BTA",
                FlightStatus.SCHEDULED,
                List.of(1L, 2L, 3L)
        );
        flight2Dto = new FlightDto(
                2L,
                "BTA01",
                1L,
                LocalDateTime.of(2023, 3, 26, 14, 30),
                LocalDateTime.of(2023, 3, 26, 14, 30),
                "BTA",
                "BRQ",
                FlightStatus.SCHEDULED,
                List.of(4L, 5L, 6L)
        );

        Pageable pageable = PageRequest.of(0, 2);
        Page<Flight> flightPage = new PageImpl<>(List.of(flight1, flight2));

        lenient().when(service.createFlight(flight1)).thenReturn(flight1);
        lenient().when(service.updateFlight(flight1)).thenReturn(flight1);
        lenient().when(service.getAllFlights(pageable)).thenReturn(flightPage);
        lenient().when(service.getFlightById(1L)).thenReturn((flight1));
        lenient().when(mapper.toDto(flight1)).thenReturn(flight1Dto);
        lenient().when(mapper.toDto(flight2)).thenReturn(flight2Dto);
        lenient().when(mapper.toEntity(flight1Dto)).thenReturn(flight1);
        lenient().when(mapper.toEntity(flight2Dto)).thenReturn(flight2);
    }

    @Test
    public void testGetAllFlights() {
        Pageable pageable = PageRequest.of(0, 2);
        List<FlightDto> flights = flightController.getAllFlights(pageable);
        assertEquals(2, flights.size());
        assertEquals(flight1Dto, flights.get(0));
        assertEquals(flight2Dto, flights.get(1));
    }

    @Test
    public void testGetFlightById() {
        FlightDto flight = flightController.getFlightById(1L);
        assertEquals(flight1Dto, flight);
    }

    @Test
    public void testCreateFlight() {
        FlightDto createdFlight = flightController.createFlight(flight1Dto);
        assertEquals(flight1Dto, createdFlight);
    }

    @Test
    public void testUpdateFlight() {
        FlightDto updatedFlight = flightController.updateFlight(flight1Dto);
        assertEquals(flight1Dto, updatedFlight);
    }

    @Test
    public void testDeleteFlight() {
        ResponseEntity<?> response = flightController.deleteFlight(1L);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        verify(service, times(1)).removeFlightById(1L);
    }
}