package cz.muni.fi.pa165.manager;

import cz.muni.fi.pa165.manager.entity.Destination;
import cz.muni.fi.pa165.manager.mapper.DestinationMapper;
import cz.muni.fi.pa165.manager.mapper.DestinationMapperImpl;
import cz.muni.fi.pa165.model.dto.DestinationDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class DestinationMapperTests {

    @InjectMocks
    private DestinationMapper mapper = new DestinationMapperImpl();

    @Test
    public void testToDto() {
        var destination = new Destination(1L, "Slovakia", "Bratislava", "BTA");
        var destinationDto = mapper.toDto(destination);

        assertEquals(destinationDto.getId(), destination.getId());
        assertEquals(destinationDto.getCity(), destination.getCity());
        assertEquals(destinationDto.getCountry(), destination.getCountry());
        assertEquals(destinationDto.getAirportCode(), destination.getAirportCode());
    }

    @Test
    public void testToEntity() {
        var destinationDto = new DestinationDto(1L, "Slovakia", "Bratislava", "BTA");
        var destination = mapper.toEntity(destinationDto);

        assertEquals(destinationDto.getId(), destination.getId());
        assertEquals(destinationDto.getCity(), destination.getCity());
        assertEquals(destinationDto.getCountry(), destination.getCountry());
        assertEquals(destinationDto.getAirportCode(), destination.getAirportCode());
    }
}
