package cz.muni.fi.pa165.manager;

import cz.muni.fi.pa165.manager.entity.Flight;
import cz.muni.fi.pa165.manager.mapper.FlightMapper;
import cz.muni.fi.pa165.manager.mapper.FlightMapperImpl;
import cz.muni.fi.pa165.model.dto.FlightDto;
import cz.muni.fi.pa165.model.types.FlightStatus;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class FlightMapperTests {

    @InjectMocks
    private FlightMapper mapper = new FlightMapperImpl();

    @Test
    public void testToDto() {
        var destination = new Flight(
                1L,
                "BRQ01",
                1L,
                LocalDateTime.of(2023, 3, 26, 14, 30),
                LocalDateTime.of(2023, 3, 26, 14, 30),
                "BRQ",
                "BTA",
                FlightStatus.SCHEDULED,
                List.of(1L, 2L, 3L)
        );
        var destinationDto = mapper.toDto(destination);

        assertEquals(destinationDto.getId(), destination.getId());
        assertEquals(destinationDto.getName(), destination.getName());
        assertEquals(destinationDto.getPlane(), destination.getPlane());
        assertEquals(destinationDto.getDeparture(), destination.getDeparture());
        assertEquals(destinationDto.getArrival(), destination.getArrival());
        assertEquals(destinationDto.getAirportFrom(), destination.getAirportFrom());
        assertEquals(destinationDto.getAirportTo(), destination.getAirportTo());
        assertEquals(destinationDto.getFlightStatus(), destination.getFlightStatus());
        assertEquals(destinationDto.getStaff(), destination.getStaff());
    }

    @Test
    public void testToEntity() {
        var destinationDto = new FlightDto(
                1L,
                "BRQ01",
                1L,
                LocalDateTime.of(2023, 3, 26, 14, 30),
                LocalDateTime.of(2023, 3, 26, 14, 30),
                "BRQ",
                "BTA",
                FlightStatus.SCHEDULED,
                List.of(1L, 2L, 3L)
        );
        var destination = mapper.toEntity(destinationDto);

        assertEquals(destinationDto.getId(), destination.getId());
        assertEquals(destinationDto.getName(), destination.getName());
        assertEquals(destinationDto.getPlane(), destination.getPlane());
        assertEquals(destinationDto.getDeparture(), destination.getDeparture());
        assertEquals(destinationDto.getArrival(), destination.getArrival());
        assertEquals(destinationDto.getAirportFrom(), destination.getAirportFrom());
        assertEquals(destinationDto.getAirportTo(), destination.getAirportTo());
        assertEquals(destinationDto.getFlightStatus(), destination.getFlightStatus());
        assertEquals(destinationDto.getStaff(), destination.getStaff());
    }
}
