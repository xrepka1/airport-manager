package cz.muni.fi.pa165.manager;

import cz.muni.fi.pa165.manager.entity.Flight;
import cz.muni.fi.pa165.manager.repository.FlightRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class FlightRepositoryTest {

    @Mock
    private FlightRepository flightRepositoryMock;

    @InjectMocks
    private Flight flight;

    @Before
    public void setUp() {
        flight = Flight.builder()
                .name("Test Flight")
                .plane(1L)
                .departure(LocalDateTime.now())
                .arrival(LocalDateTime.now().plusHours(2))
                .airportFrom("Prague")
                .airportTo("London")
                .build();
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void shouldFindFlightByName() {
        when(flightRepositoryMock.getFlightByName("Test Flight")).thenReturn(Optional.of(flight));
        Optional<Flight> foundFlight = flightRepositoryMock.getFlightByName("Test Flight");
        assertThat(foundFlight).isPresent();
        assertThat(foundFlight.get()).isEqualTo(flight);
    }
}