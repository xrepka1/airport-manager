package cz.muni.fi.pa165.manager;
import cz.muni.fi.pa165.manager.entity.Destination;
import cz.muni.fi.pa165.manager.repository.DestinationRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@DataJpaTest
public class DestinationRepositoryTest {

    @Mock
    private DestinationRepository destinationRepository;

    @Before
    public void setUp() {
        Destination destination = new Destination();
        destination.setAirportCode("LHR");
        when(destinationRepository.getDestinationByAirportCode(anyString()))
                .thenReturn(Optional.of(destination));
    }

    @Test
    public void testGetDestinationByAirportCode() {
        Optional<Destination> destinationOptional = destinationRepository.getDestinationByAirportCode("LHR");

        assertThat(destinationOptional).isPresent();
        assertThat(destinationOptional.get().getAirportCode()).isEqualTo("LHR");
    }

}