package cz.muni.fi.pa165.manager;

import cz.muni.fi.pa165.manager.entity.Flight;
import cz.muni.fi.pa165.manager.repository.DestinationRepository;
import cz.muni.fi.pa165.manager.repository.FlightRepository;
import cz.muni.fi.pa165.manager.service.FlightManagerService;
import cz.muni.fi.pa165.model.types.FlightStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@DataJpaTest
class FlightTests {
    @Mock
    private DestinationRepository destinationRepository;

    @Mock
    private FlightRepository flightRepository;

    @InjectMocks
    private FlightManagerService service;

    private final LocalDateTime departureTime = LocalDateTime.of(2023, 3, 26, 14, 30);
    private final LocalDateTime arrivalTime = LocalDateTime.of(2023, 3, 26, 15, 0);
    private final Flight flight = new Flight(
            1L,
            "BRQ01",
            1L,
            departureTime,
            arrivalTime,
            "BRQ",
            "BTA",
            FlightStatus.SCHEDULED,
            List.of(1L, 2L, 3L)
    );
    private final Flight flight_empty = new Flight();

    @BeforeEach
    public void setup() {
        service = new FlightManagerService(flightRepository, destinationRepository);
        flight_empty.setId(1L);
    }

    @Test
    public void testCreateFlight() {
        when(flightRepository.save(any(Flight.class))).thenReturn(flight);

        Flight createdFlight = service.createFlight(flight);
        assertNotNull(createdFlight);
        assertEquals(createdFlight.getId(), flight.getId());
        assertEquals(createdFlight.getName(), flight.getName());
        assertEquals(createdFlight.getPlane(), flight.getPlane());
        assertEquals(createdFlight.getDeparture(), flight.getDeparture());
        assertEquals(createdFlight.getArrival(), flight.getArrival());
        assertEquals(createdFlight.getAirportFrom(), flight.getAirportFrom());
        assertEquals(createdFlight.getAirportTo(), flight.getAirportTo());
        assertEquals(createdFlight.getFlightStatus(), flight.getFlightStatus());
        assertEquals(createdFlight.getStaff(), flight.getStaff());
    }

    @Test
    public void testGetAllFlights() {
        Pageable pageable = PageRequest.of(0, 2);
        Page<Flight> flightPage = new PageImpl<>(List.of(flight, flight_empty));

        // When findAll is called on the repository with any Pageable, return the flightPage.
        when(flightRepository.findAll(any(Pageable.class))).thenReturn(flightPage);

        Page<Flight> allFlights = service.getAllFlights(pageable);

        assertNotNull(allFlights);
        assertEquals(2, allFlights.getTotalElements());
        assertEquals(flight, allFlights.getContent().get(0));
        assertEquals(flight_empty, allFlights.getContent().get(1));
    }

    @Test
    public void testFindById() {
        when(flightRepository.findById(0L)).thenReturn(Optional.of(flight));

        Flight flightById = service.getFlightById(0L);
        assertNotNull(flightById);
        assertEquals(flightById, flight);
    }

    @Test
    public void testFindById2() {
        when(flightRepository.findById(1L)).thenReturn(Optional.of(flight_empty));

        Flight flightById = service.getFlightById(1L);
        assertNotNull(flightById);
        assertEquals(flightById, flight_empty);
    }

    @Test
    public void testUpdateFlight() {
        Flight updatedFlight = new Flight();

        updatedFlight.setId(0L);
        updatedFlight.setName("Brno-Bratislava");
        updatedFlight.setPlane(2L);
        updatedFlight.setDeparture(departureTime);
        updatedFlight.setArrival(arrivalTime);
        updatedFlight.setAirportFrom("BTA");
        updatedFlight.setAirportTo("BRQ");
        updatedFlight.setFlightStatus(FlightStatus.ARRIVED);
        updatedFlight.setStaff(List.of(4L, 5L, 6L));

        when(flightRepository.findById(0L)).thenReturn(Optional.of(flight));
        when(flightRepository.save(any(Flight.class))).thenReturn(updatedFlight);

        Flight result = service.updateFlight(updatedFlight);

        assertEquals(updatedFlight.getId(), result.getId());
        assertEquals(updatedFlight.getName(), result.getName());
        assertEquals(updatedFlight.getFlightStatus(), result.getFlightStatus());
    }

    @Test
    public void testRemoveFlightById() {
        doNothing().when(flightRepository).deleteById(flight_empty.getId());

        service.removeFlightById(flight_empty.getId());
        verify(flightRepository, times(1)).deleteById(flight_empty.getId());
    }

}
