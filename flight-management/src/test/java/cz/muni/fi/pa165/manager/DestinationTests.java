package cz.muni.fi.pa165.manager;

import cz.muni.fi.pa165.manager.entity.Destination;
import cz.muni.fi.pa165.manager.entity.Flight;
import cz.muni.fi.pa165.manager.repository.DestinationRepository;
import cz.muni.fi.pa165.manager.repository.FlightRepository;
import cz.muni.fi.pa165.manager.service.FlightManagerService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
@DataJpaTest
public class DestinationTests {

    @Mock
    private DestinationRepository destinationRepository;

    @Mock
    private FlightRepository flightRepository;

    @InjectMocks
    private FlightManagerService service;

    private Destination destination;

    @BeforeEach
    public void setup() {
        service = new FlightManagerService(flightRepository, destinationRepository);
        destination = new Destination();
        destination.setCountry("Czech Republic");
        destination.setCity("Brno");
        destination.setAirportCode("BRQ");
    }

    @Test
    public void testCreateDestination() {
        when(destinationRepository.save(any(Destination.class))).thenReturn(destination);

        Destination createdDestination = service.createDestination(destination);
        assertNotNull(createdDestination);
        assertEquals(destination.getId(), createdDestination.getId());
        assertEquals(destination.getCity(), createdDestination.getCity());
        assertEquals(destination.getAirportCode(), createdDestination.getAirportCode());
        assertEquals(destination.getCountry(), createdDestination.getCountry());
    }

    @Test
    public void testFindAllDestinations() {
        Pageable pageable = PageRequest.of(0, 1);
        Page<Destination> destPage = new PageImpl<>(List.of(destination));

        when(destinationRepository.findAll(any(Pageable.class))).thenReturn(destPage);

        Page<Destination> destinations = service.findAllDestination(pageable);
        assertNotNull(destinations);
        assertFalse(destinations.isEmpty());
        assertEquals(1, destinations.getTotalElements());
        assertEquals(destination, destinations.getContent().get(0));
    }

    @Test
    public void testFindById() {
        when(destinationRepository.findById(1L)).thenReturn(Optional.of(destination));

        Destination foundDestination = service.findDestinationById(1L);
        assertNotNull(foundDestination);
        assertEquals(destination, foundDestination);
    }

    @Test
    public void testUpdateDestination() {
        Destination updatedDestination = new Destination();
        updatedDestination.setId(1L);
        updatedDestination.setCity("Bratislava");
        updatedDestination.setAirportCode("BTS");
        updatedDestination.setCountry("Slovakia");

        when(destinationRepository.findById(1L)).thenReturn(Optional.of(destination));
        when(destinationRepository.save(any(Destination.class))).thenReturn(updatedDestination);

        Destination result = service.updateDestination(updatedDestination);
        assertNotNull(result);
        assertEquals(updatedDestination.getId(), result.getId());
        assertEquals(updatedDestination.getCity(), result.getCity());
        assertEquals(updatedDestination.getAirportCode(), result.getAirportCode());
        assertEquals(updatedDestination.getCountry(), result.getCountry());
    }

    @Test
    public void testDeleteDestination() {
        Destination destination = new Destination();
        destination.setCountry("Czech Republic");
        destination.setCity("Brno");
        destination.setAirportCode("BRQ");


        doNothing().when(destinationRepository).deleteById(destination.getId());
        service.deleteDestination(destination.getId());
        verify(destinationRepository, times(1)).deleteById(destination.getId());
    }

}