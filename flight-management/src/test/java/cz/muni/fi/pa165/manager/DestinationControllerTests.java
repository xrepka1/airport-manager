package cz.muni.fi.pa165.manager;

import cz.muni.fi.pa165.manager.controller.destinations.DestinationController;
import cz.muni.fi.pa165.manager.entity.Destination;
import cz.muni.fi.pa165.manager.entity.Flight;
import cz.muni.fi.pa165.manager.mapper.DestinationMapper;
import cz.muni.fi.pa165.manager.service.FlightManagerService;
import cz.muni.fi.pa165.model.dto.DestinationDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class DestinationControllerTests {
    @InjectMocks
    private DestinationController destinationController;

    @Mock
    private FlightManagerService service;
    @Mock
    private DestinationMapper mapper;
    private Destination destination1;
    private DestinationDto destination1Dto;
    private Destination destination2;
    private DestinationDto destination2Dto;

    @BeforeEach
    public void setUp() {
        destination1 = new Destination(1L, "Slovakia", "Bratislava", "BTA");
        destination2 = new Destination(2L, "Czech Republic", "Brno", "BRQ");
        destination1Dto = new DestinationDto(1L, "Slovakia", "Bratislava", "BTA");
        destination2Dto = new DestinationDto(2L, "Czech Republic", "Brno", "BRQ");

        Pageable pageable = PageRequest.of(0, 2);
        Page<Destination> destinationPage = new PageImpl<>(List.of(destination1, destination2));


        lenient().when(service.createDestination(destination1)).thenReturn(destination1);
        lenient().when(service.updateDestination(destination1)).thenReturn(destination1);
        lenient().when(service.findAllDestination(pageable)).thenReturn(destinationPage);
        lenient().when(service.findDestinationById(1L)).thenReturn(destination1);
        lenient().when(mapper.toDto(destination1)).thenReturn(destination1Dto);
        lenient().when(mapper.toDto(destination2)).thenReturn(destination2Dto);
        lenient().when(mapper.toEntity(destination1Dto)).thenReturn(destination1);
        lenient().when(mapper.toEntity(destination2Dto)).thenReturn(destination2);
    }

    @Test
    public void testGetAllDestinations() {
        Pageable pageable = PageRequest.of(0, 2);
        List<DestinationDto> destinations = destinationController.findAll(pageable);
        assertEquals(2, destinations.size());
        assertEquals(destination1Dto, destinations.get(0));
        assertEquals(destination2Dto, destinations.get(1));
    }

    @Test
    public void testGetDestinationById() {
        DestinationDto destination = destinationController.findById(1L);
        assertEquals(destination1Dto, destination);
    }

    @Test
    public void testCreateDestination() {
        DestinationDto createdDestination = destinationController.create(destination1Dto);
        assertEquals(destination1Dto, createdDestination);
    }

    @Test
    public void testUpdateDestination() {
        DestinationDto updatedDestination = destinationController.update(destination1Dto);
        assertEquals(destination1Dto, updatedDestination);
    }

    @Test
    public void testDeleteDestination() {
        ResponseEntity<?> response = destinationController.delete(1L);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        verify(service, times(1)).deleteDestination(1L);
    }
}