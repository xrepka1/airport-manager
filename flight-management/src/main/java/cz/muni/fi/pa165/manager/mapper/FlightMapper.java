package cz.muni.fi.pa165.manager.mapper;

import cz.muni.fi.pa165.manager.entity.Flight;
import cz.muni.fi.pa165.model.dto.FlightDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface FlightMapper{
    Flight toEntity(FlightDto flightDto);
    FlightDto toDto(Flight flight);
}
