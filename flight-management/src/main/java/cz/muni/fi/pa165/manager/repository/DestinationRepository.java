package cz.muni.fi.pa165.manager.repository;

import cz.muni.fi.pa165.manager.entity.Destination;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DestinationRepository extends JpaRepository<Destination, Long> {
    @Query(value = "select f from Destination f where f.airportCode = :code")
    Optional<Destination> getDestinationByAirportCode(@Param("code") String airportCode);
}
