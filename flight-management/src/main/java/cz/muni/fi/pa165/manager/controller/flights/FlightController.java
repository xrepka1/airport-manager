package cz.muni.fi.pa165.manager.controller.flights;

import cz.muni.fi.pa165.manager.mapper.FlightMapper;
import cz.muni.fi.pa165.manager.service.FlightManagerService;
import cz.muni.fi.pa165.model.dto.FlightDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/flight")
@Tag(name = "Flights", description = "Operations related to flights")
public class FlightController {
    private final FlightManagerService service;
    private final FlightMapper mapper;

    @Autowired
    public FlightController(FlightManagerService service, FlightMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @GetMapping
    @Operation(summary = "Get all flights")
    public List<FlightDto> getAllFlights(Pageable pageable) {
        var flights = service.getAllFlights(pageable);
        List<FlightDto> result = new ArrayList<>();
        for (var flight : flights) {
            result.add(mapper.toDto(flight));
        }
        return result;
    }

    @GetMapping("/{id}")
    @Operation(summary = "Get flight by ID")
    public FlightDto getFlightById(@PathVariable(value = "id") @Parameter(description = "Flight ID") Long id) {
        return mapper.toDto(service.getFlightById(id));
    }

    @GetMapping("/layover")
    @Operation(summary = "Get flights layover by flights IDs")
    public Long getFlightsLayover(
            @Parameter(description = "First flight ID") Long firstFlightId,
            @Parameter(description = "Second flight ID") Long secondFlightId
    ) {
        return service.findConnectingFlights(firstFlightId, secondFlightId);
    }

    @PostMapping
    @Operation(summary = "Create a new flight")
    public FlightDto createFlight(@Valid @RequestBody @Parameter(description = "Flight details") FlightDto flight) {
        return mapper.toDto(service.createFlight(mapper.toEntity(flight)));
    }
    @PutMapping
    @Operation(summary = "Update flight")
    public FlightDto updateFlight(@Valid @RequestBody @Parameter(description = "Flight details") FlightDto flight) {
        return mapper.toDto(service.updateFlight(mapper.toEntity(flight)));
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Delete flight by ID")
    public ResponseEntity<?> deleteFlight(@PathVariable(value = "id") @Parameter(description = "Flight ID") Long id) {
        service.removeFlightById(id);
        return ResponseEntity.ok().build();
    }
}
