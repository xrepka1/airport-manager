package cz.muni.fi.pa165.manager;

import cz.muni.fi.pa165.Parser;
import cz.muni.fi.pa165.manager.entity.Destination;
import cz.muni.fi.pa165.manager.entity.Flight;
import cz.muni.fi.pa165.manager.repository.DestinationRepository;
import cz.muni.fi.pa165.manager.repository.FlightRepository;
import cz.muni.fi.pa165.model.types.FlightStatus;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

@Component
public class DbInit {

    @Autowired
    private DestinationRepository destinationRepository;
    @Autowired
    private FlightRepository flightRepository;

    @PostConstruct
    private void postConstruct() throws IOException {
        List<Map<String, String>> destEntities = Parser.parseFromCSV(
                "./src/main/java/cz/muni/fi/pa165/manager/data/destination.csv"
        );

        List<Map<String, String>> flightEntities = Parser.parseFromCSV(
                "./src/main/java/cz/muni/fi/pa165/manager/data/flight.csv"
        );

        for (Map<String, String> rawEntity : destEntities) {

            Destination destination = new Destination();

            destination.setAirportCode(rawEntity.get("airportCode"));
            destination.setCountry(rawEntity.get("country"));
            destination.setCity(rawEntity.get("city"));

            destinationRepository.save(destination);
        }

        for (Map<String, String> rawEntity : flightEntities) {

            Flight flight = new Flight();

            flight.setName(rawEntity.get("name"));
            flight.setPlane(Long.valueOf(rawEntity.get("plane")));
            flight.setDeparture(LocalDateTime.parse(rawEntity.get("departure")));
            flight.setArrival(LocalDateTime.parse(rawEntity.get("arrival")));
            flight.setAirportFrom(rawEntity.get("airportFrom"));
            flight.setAirportTo(rawEntity.get("airportTo"));
            flight.setFlightStatus(FlightStatus.valueOf(rawEntity.get("flightStatus")));
            flight.setStaff(generateStaffArrayList());

            flightRepository.save(flight);
        }

    }

    private static List<Long> generateStaffArrayList(){
        List<Long> randomNumbers = new ArrayList<>();
        Random rand = new Random();

        for (int i = 0; i < 4; i++) {
            randomNumbers.add(rand.nextLong(1, 100));
        }

        return randomNumbers;
    }
}