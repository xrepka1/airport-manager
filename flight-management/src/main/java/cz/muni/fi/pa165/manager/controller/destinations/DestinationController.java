package cz.muni.fi.pa165.manager.controller.destinations;

import cz.muni.fi.pa165.manager.mapper.DestinationMapper;
import cz.muni.fi.pa165.manager.service.FlightManagerService;
import cz.muni.fi.pa165.model.dto.DestinationDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/destination")
@Tag(name = "destination", description = "Operations about destinations")
public class DestinationController {
    private final FlightManagerService service;
    private final DestinationMapper destinationMapper;

    @Autowired
    public DestinationController(FlightManagerService service, DestinationMapper destinationMapper) {
        this.service = service;
        this.destinationMapper = destinationMapper;
    }

    @Operation(summary = "Creates a new destination")
    @PostMapping
    public DestinationDto create(@Valid @RequestBody @Parameter(description = "Flight details") DestinationDto destination) {
        return destinationMapper.toDto(service.createDestination(destinationMapper.toEntity(destination)));
    }

    @Operation(summary = "Updates an existing destination")
    @PutMapping
    public DestinationDto update(@Valid @RequestBody DestinationDto destination) {
        return destinationMapper.toDto(service.updateDestination(destinationMapper.toEntity(destination)));
    }

    @Operation(summary = "Deletes an existing destination")
    @Parameter(name = "id", allowEmptyValue = false, description = "Id of the destination")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable(value = "id") Long id) {
        service.deleteDestination(id);
        return ResponseEntity.ok().build();
    }

    @Operation(summary = "Get all existing destinations")
    @GetMapping
    public List<DestinationDto> findAll(Pageable pageable) {
        var destinations = service.findAllDestination(pageable);
        List<DestinationDto> result = new ArrayList<>();
        for (var dest : destinations) {
            result.add(destinationMapper.toDto(dest));
        }
        return result;
    }

    @Operation(summary = "Gets an existing destination by id")
    @Parameter(name = "id", description = "Id of the destination")
    @GetMapping("/{id}")
    public DestinationDto findById(@PathVariable(value = "id") Long id) {
        return destinationMapper.toDto(service.findDestinationById(id));
    }
}

