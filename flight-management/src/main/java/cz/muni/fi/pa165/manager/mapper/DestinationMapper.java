package cz.muni.fi.pa165.manager.mapper;

import cz.muni.fi.pa165.manager.entity.Destination;
import cz.muni.fi.pa165.model.dto.DestinationDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface DestinationMapper {
    Destination toEntity(DestinationDto destinationDto);
    DestinationDto toDto(Destination destination);
}
