package cz.muni.fi.pa165.manager.repository;

import cz.muni.fi.pa165.manager.entity.Flight;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface FlightRepository extends JpaRepository<Flight, Long> {
    @Query(value = "select f from Flight f where f.name = :name")
    Optional<Flight> getFlightByName(@Param("name") String name);
}
