package cz.muni.fi.pa165.manager.service;

import cz.muni.fi.pa165.manager.entity.Destination;
import cz.muni.fi.pa165.manager.entity.Flight;
import cz.muni.fi.pa165.manager.repository.DestinationRepository;
import cz.muni.fi.pa165.manager.repository.FlightRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Duration;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class FlightManagerService {

    private final FlightRepository flightRepository;
    private final DestinationRepository destinationRepository;

    @Autowired
    public FlightManagerService(FlightRepository flightRepository, DestinationRepository destinationRepository) {
        this.flightRepository = flightRepository;
        this.destinationRepository = destinationRepository;
    }

    public Flight createFlight(Flight flight) {
        Optional<Flight> oldFlightOptional = flightRepository.getFlightByName(flight.getName());

        if (oldFlightOptional.isPresent()) {
            Flight oldFlight = oldFlightOptional.get();

            if (flight.getDeparture().isEqual(oldFlight.getDeparture()) ||
                    (flight.getDeparture().isAfter(oldFlight.getDeparture()) &&
                            flight.getDeparture().isBefore(oldFlight.getArrival()))) {
                throw new IllegalArgumentException("Flight with name " + flight.getName() + " is already scheduled within the specified timeframe!");
            }
        }

        return flightRepository.save(flight);
    }

    public Long findConnectingFlights(Long flightId1, Long flightId2) {
        Flight firstFlight = getFlightById(flightId1);
        Flight secondFlight = getFlightById(flightId2);

        if (!firstFlight.getAirportTo().equals(secondFlight.getAirportFrom())) {
            throw new IllegalArgumentException("Given flights are not connected.");
        }

        long layover = Duration.between(
                firstFlight.getArrival(),
                secondFlight.getDeparture()
        ).toMinutes();

        if (layover < 0) {
            throw new IllegalArgumentException("The first flight arrives after the second's departure.");
        }

        return layover;
    }

    public Page<Flight> getAllFlights(Pageable pageable) {
        return flightRepository.findAll(pageable);
    }

    public Flight getFlightById(Long id) {
        return flightRepository.findById(id)
                .orElseThrow(() -> new NoSuchElementException("Flight with provided ID not found."));
    }

    public void removeFlightById(Long id) {
        flightRepository.deleteById(id);
    }

    public Flight updateFlight(Flight flight) {
        if (flightRepository.findById(flight.getId()).isPresent()) {
            return flightRepository.save(flight);
        }

        throw new NoSuchElementException("Flight with provided ID not found.");
    }

    @Transactional
    public Destination createDestination(Destination destination) {
        if (destinationRepository.getDestinationByAirportCode(destination.getAirportCode()).isPresent()) {
            throw new IllegalArgumentException("Destination with id " + destination.getId() + " already exists");
        }

        return destinationRepository.save(destination);
    }

    public Destination updateDestination(Destination destination) {
        if (destinationRepository.findById(destination.getId()).isPresent()) {
            return destinationRepository.save(destination);
        }

        throw new NoSuchElementException("Destination with provided ID not found.");
    }

    public void deleteDestination(Long id) {
        destinationRepository.deleteById(id);
    }

    public Page<Destination> findAllDestination(Pageable pageable) {
        return destinationRepository.findAll(pageable);
    }

    public Destination findDestinationById(Long id) {
        return destinationRepository.findById(id)
                .orElseThrow(() -> new NoSuchElementException("Destination with provided ID not found."));
    }
}
