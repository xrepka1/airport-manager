package cz.muni.fi.pa165.manager.entity;

import cz.muni.fi.pa165.model.types.FlightStatus;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Flight {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private Long plane;
    private LocalDateTime departure;
    private LocalDateTime arrival;
    private String airportFrom;
    private String airportTo;
    private FlightStatus flightStatus;
    private List<Long> staff;
}