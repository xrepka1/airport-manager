import time
import sys

from locust import FastHttpUser, between, task

sys.stdout.reconfigure(encoding="utf-8")

class FlightUser(FastHttpUser):
    wait_time = between(1000, 6000)
    host = "http://localhost:8081"

    def on_start(self):
        file = open("accessToken.txt", "r")
        self.access_token = file.read()
        self.plane_api = "/api/flight"
        self.destination_api = "/api/destination"
        self.client.headers = {'content-type' : 'application/json',
            'Authorization' : f'Bearer {self.access_token}'}
        self.client.host = "http://127.0.0.1:8081"

    @task(10)
    def get_and_update_flight(self):
        flight_id = 7
        flight_name = "Lacov let"
        with self.client.get(
            f"{self.plane_api}/{flight_id}"
        ) as response:
            if response.status_code == 200:
                resp_json = response.json()
                resp_json['name'] = flight_name
                with self.client.put(
                    f"{self.host}{self.plane_api}",
                    json=resp_json
                ) as response_put:
                    if response_put.status_code != 200:
                        print("Failed to update flight.")
                        return -1
                    print("Flight updated")
            else:
                print("Failed to get flight details.")

    @task(10)
    def get_destination_info(self):
        with self.client.get(
            self.host + self.destination_api
        ) as response:
            if response.status_code == 200:
                destinations = response.json()
                if not destinations:
                    print("Empty destination details.")
                    return -1
                print(destinations)
            else:
                print("Failed to get destination details.")

if __name__ == "__main__":
    run_single_user(FlightUser)
