import time
from locust import FastHttpUser, between, task
from datetime import datetime, timedelta

class ReporterUser(FastHttpUser):
    wait_time = between(1000, 6000)
    host = "http://localhost:8083"

    def on_start(self):
        file = open("accessToken.txt", "r")
        self.access_token = file.read()
        self.reporter_api = "/api/reporter"
        self.client.headers = {'content-type' : 'application/json',
            'Authorization' : f'Bearer {self.access_token}'}
        self.client.host = "http://127.0.0.1:8083"

    @task
    def create_report(self):
        print(self.client.headers)
        body = {
                "reportId": 1,
                "flightId": 12,
                "departureDelay": "00:20:15",
                "arrivalDelay": "00:15:10",
                "message": "Cleared sky helped to decrease delay"
            }
        with self.client.post(f"{self.host}{self.reporter_api}/id/",
            json = body
        ) as response:
            if response.status_code != 200:
                print("Internal server error")
                return -1
            body = response.json()
            if not body:
                print("Failed to create report.")
                return -1
            print(body)


    @task
    def get_and_update_report(self):
        id = 5
        with self.client.get(f"{self.host}{self.reporter_api}/id/{id}"
        ) as response:
            if response.status_code != 200:
                print("Internal server error")
                return -1
            body = response.json()
            if not body:
                print("Report not found")
                return -1
            print("Found report: " + body)
            newDeparture  = datetime.strptime(body['departureDelay'], "%H:%M:%S") + timedelta(hours(1))
            newArrival  = datetime.strptime(body['arrivalDelay'], "%H:%M:%S") + timedelta(hours(1))
            body['departureDelay'] = newDeparture.strftime('%H:%M:%S')
            body['arrivalDelay'] = newArrival.strftime('%H:%M:%S')
            with self.client.get(f"{self.host}{self.reporter_api}/id/{id}",
                json = body
            ) as response:
                if response.status_code != 200:
                    print("Internal server error")
                    return -1
                body = response.json()
                if not body:
                    response.failure("Report not found")
                    return -1
                print("Updated report: " + body)


if __name__ == "__main__":
    run_single_user(FlightUser)