import time
from locust import FastHttpUser, between, task
from datetime import datetime, timedelta

class ReporterUser(FastHttpUser):
    wait_time = between(1000, 6000)
    host = "http://localhost:8084"

    access_token = ""
    def on_start(self):
        file = open("accessToken.txt", "r")
        self.access_token = file.read()
        self.staff_api = "/api/resource-management/staff"
        self.plane_api = "/api/resource-management/planes"
        self.client.headers = {'content-type' : 'application/json',
            'Authorization' : f'Bearer {self.access_token}'}
        self.client.host = "http://127.0.0.1:8084"

    @task(10)
    def delete_plane(self):
        resp = curl()
        id = 11
        with self.client.post(f"{self.host}{self.plane_api}/id/{id}") as response:
            if response.status_code != 200:
                print("Plane not deleted")
                return -1
            print("Plane with ID " + f"{id}" + " deleted")

    @task(10)
    def get_and_update_staff(self):
        id = 4
        with self.client.get(f"{self.host}{self.staff_api}/id/{id}") as response:
            if response.status_code != 200:
                print("Internal server error")
                return -1
            created = response.json()
            if not created:
                print("Report not found")
                return -1
            staff = response.json()
            print("Found staff: " + staff)
            staff['lasName'] = "Smith"
            with self.client.get(f"{self.host}{self.staff_api}" + "/id/" + f"{id}",
                json = staff
            ) as response:
                if response.status_code != 200:
                    print("Internal server error")
                    return -1
                newStaff = response.json()
                if not newStaff:
                    response.failure("Report not found")
                    return -1
                print("Updated report: " + newStaff)


if __name__ == "__main__":
    run_single_user(FlightUser)