package cz.muni.fi.pa165.reporter.service;

import cz.muni.fi.pa165.reporter.entity.Reporter;
import cz.muni.fi.pa165.reporter.repository.ReporterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalTime;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class ReporterService {

    private final ReporterRepository reporterRepository;

    @Autowired
    public ReporterService(ReporterRepository reporterRepository) {
        this.reporterRepository = reporterRepository;
    }
    @Transactional
    public Reporter createReporter(Reporter reporter) {
        return reporterRepository.save(reporter);
    }

    @Transactional(readOnly = true)
    public List<Reporter> getAllReporters() {
        return reporterRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Reporter getReporterById(Long reportId) {
        return reporterRepository.findById(reportId)
                .orElseThrow(() -> new NoSuchElementException("Report not found with reportId " + reportId));
    }

    @Transactional
    public void removeReporterById(Long reportId) {
        reporterRepository.deleteById(reportId);
    }

    @Transactional
    public Reporter updateReport(Long reportId, Reporter reporter) {
        Reporter existingReporter = reporterRepository.findById(reportId)
                .orElseThrow(() -> new NoSuchElementException("Report not found with reportId " + reporter.getReportId()));
        existingReporter.setFlightId(reporter.getFlightId());
        existingReporter.setMessage(reporter.getMessage());
        existingReporter.setArrivalDelay(reporter.getArrivalDelay());
        existingReporter.setDepartureDelay(reporter.getDepartureDelay());

        return reporterRepository.save(existingReporter);
    }

    @Transactional(readOnly = true)
    public List<Reporter> getReportersWithDepartureDelayOver(LocalTime departureDelay) {return reporterRepository.findAllByDepartureDelayOver(departureDelay);}

    @Transactional(readOnly = true)
    public List<Reporter> getReportersWithArrivalDelayOver(LocalTime arrivalDelay) {return reporterRepository.findAllByArrivalDelayOver(arrivalDelay);}

    @Transactional(readOnly = true)
    public Reporter getReporterByFlightId(Long flightId) {return reporterRepository.findByFlightId(flightId);}


    /*
    **  Can be optimized by SQL query pre-computation, JpaRepository don't allow query as input parameter.
    *   Figure out the way around.
     */
    @Transactional(readOnly = true)
    public List<Reporter> getReportersByQuery(String query){
        Pattern departureMinDelayPattern = Pattern.compile("departureDelayMin=[0-9]{2}:[0-9]{2}:[0-9]{2}");
        Pattern departureMaxDelayPattern = Pattern.compile("departureDelayMax=[0-9]{2}:[0-9]{2}:[0-9]{2}");
        Pattern arrivalMinDelayPattern = Pattern.compile("arrivalDelayMin=[0-9]{2}:[0-9]{2}:[0-9]{2}");
        Pattern arrivalMaxDelayPattern = Pattern.compile("arrivalDelayMax=[0-9]{2}:[0-9]{2}:[0-9]{2}");
        var reports = reporterRepository.findAll();
        Matcher depMin = departureMinDelayPattern.matcher(query);
        if (depMin.find()) {
            reports.removeIf(x -> x.getDepartureDelay().isBefore(LocalTime.parse(depMin.group(0).split("=")[1])));
        }
        Matcher depMax = departureMaxDelayPattern.matcher(query);
        if (depMax.find()) {
            reports.removeIf(x -> x.getDepartureDelay().isAfter(LocalTime.parse(depMax.group(0).split("=")[1])));
        }
        Matcher arrMin = arrivalMinDelayPattern.matcher(query);
        if (arrMin.find()) {
            reports.removeIf(x -> x.getArrivalDelay().isBefore(LocalTime.parse(arrMin.group(0).split("=")[1])));
        }
        Matcher arrMax = arrivalMaxDelayPattern.matcher(query);
        if (arrMax.find()) {
            reports.removeIf(x -> x.getArrivalDelay().isAfter(LocalTime.parse(arrMax.group(0).split("=")[1])));
        }
        return reports;
    }
}
