package cz.muni.fi.pa165.reporter.mapper;

import cz.muni.fi.pa165.reporter.entity.Reporter;
import cz.muni.fi.pa165.reporter.entity.ReporterDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ReporterMapper{
    Reporter toEntity(ReporterDto dto);

    ReporterDto toDto(Reporter entity);
}
