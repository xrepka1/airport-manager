package cz.muni.fi.pa165.reporter.controller;

import cz.muni.fi.pa165.reporter.entity.Reporter;
import cz.muni.fi.pa165.reporter.entity.ReporterDto;
import cz.muni.fi.pa165.reporter.mapper.ReporterMapper;
import cz.muni.fi.pa165.reporter.service.ReporterService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("api/reporter")
@Tag(name = "Reporter", description = "Report management system")
public class ReporterController {

    private final ReporterService reporterService;
    private final ReporterMapper reporterMapper;

    @Autowired
    public ReporterController(ReporterService service, ReporterMapper mapper) {
        this.reporterService = service;
        this.reporterMapper = mapper;
    }

    @GetMapping
    @Operation(summary = "Get all reporters")
    public ResponseEntity<List<ReporterDto>> getAllReporters() {
        var reporters = reporterService.getAllReporters();
        return getListReporterEntity(reporters);
    }

    @GetMapping("/id/{reportId}")
    @Operation(summary = "Get reporter by ID")
    public ResponseEntity<ReporterDto> getReporterById(@Valid @PathVariable(value = "reportId") @Parameter(description = "Report ID") Long reportId) {
        return ResponseEntity.ok(reporterMapper.toDto(reporterService.getReporterById(reportId)));
    }

    @PostMapping("/id/")
    @Operation(summary = "Create a new reporter")
    public ResponseEntity<ReporterDto> createReporter(@Valid @RequestBody @Parameter(description = "Report details")
                                    ReporterDto reporterDto) {
        return ResponseEntity.ok(reporterMapper.toDto(reporterService.createReporter(reporterMapper.toEntity(reporterDto))));
    }

    @PutMapping("/id/{reportId}")
    @Operation(summary = "Update reporter by ID")
    public ResponseEntity<ReporterDto> updateReporter(@Valid @PathVariable(value = "reportId") @Parameter(description = "Report ID") Long reporterId,
                                 @Valid @RequestBody
                               @Parameter(description = "Updated report details") ReporterDto reporter) {
        return ResponseEntity.ok(reporterMapper.toDto(reporterService.updateReport(reporterId , reporterMapper.toEntity(reporter))));
    }

    @DeleteMapping("/id/{reportId}")
    @Operation(summary = "Delete reporter by ID")
    public ResponseEntity<?> deleteReporter(@Valid @PathVariable(value = "reportId") @Parameter(description = "Report ID") Long reportId) {
        reporterService.removeReporterById(reportId);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/departure-delay/{departureDelay}")
    @Operation(summary = "Get reporters with Departure delay over DELAY")
    public ResponseEntity<List<ReporterDto>> getReportersWithDepartureDelayOver(@PathVariable(value = "departureDelay") @Parameter(description = "Delay time", example = "00:00:00") String departureDelay) {
        var reporters = reporterService.getReportersWithDepartureDelayOver(LocalTime.parse(departureDelay));
        return getListReporterEntity(reporters);
    }

    @GetMapping("/arrival-delay/{arrivalDelay}")
    @Operation(summary = "Get reporters with Arrival delay over DELAY")
    public ResponseEntity<List<ReporterDto>> getReportersWithArrivalDelayOver(@PathVariable(value = "arrivalDelay") @Parameter(description = "Delay time", example = "00:00:00") String arrivalDelay) {
        var reporters = reporterService.getReportersWithArrivalDelayOver(LocalTime.parse(arrivalDelay));
        return getListReporterEntity(reporters);
    }

    @GetMapping("/flight-id/{flightId}")
    @Operation(summary = "Get reporters with given flightId")
    public ResponseEntity<ReporterDto> getReporterByFlightId(@Valid @PathVariable(value = "flightId") @Parameter(description = "Flight ID") Long flightId) {
        return ResponseEntity.ok(reporterMapper.toDto(reporterService.getReporterByFlightId(flightId)));
    }

    @GetMapping("/download/{query}")
    @Operation(summary = "Download file containing reporters find by given query")
    public ResponseEntity<?> downloadReportersFileByQuery(@PathVariable(value = "query")
                                                 @Parameter(description = "Search query, omit query parameter if not used," +
                                                         " used parameters: departureDelayMin, departureDelayMax, " +
                                                         "arrivalDelayMin, arrivalDelayMax", example = "departureDelayMin=00:10:00,departureDelayMax=01:00:00")
                                                 String query) throws IOException {
        Path path = Path.of("reporter.txt");
        try {
            FileWriter myWriter = new FileWriter(path.toString());
            myWriter.write(reporterService.getReportersByQuery(query).toString());
            myWriter.close();
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Problem with file handling");
        }
        Resource resource = new UrlResource(path.toUri());
        if (!resource.exists())
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body("File not created");
        String contentType = "application/octet-stream";
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }

    private ResponseEntity<List<ReporterDto>> getListReporterEntity(List<Reporter> reporters) {
        if (reporters.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        List<ReporterDto> result = new ArrayList<>();
        for (var reporter : reporters) {
            result.add(reporterMapper.toDto(reporter));
        }
        return ResponseEntity.ok(result);
    }
}
