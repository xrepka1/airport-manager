package cz.muni.fi.pa165.reporter.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Component
public class Reporter {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long reportId;
    private Long flightId;
    private LocalTime departureDelay;
    private LocalTime arrivalDelay;
    private String message;

    @Override
    public String toString() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss");
        return "Report{" +
                "reportId=" + reportId +
                ", flightId=" + flightId +
                ", departureDelay=" + departureDelay.format(dtf) +
                ", arrivalDelay=" + arrivalDelay.format(dtf) +
                ", message='" + message + '\'' +
                '}';
    }
}
