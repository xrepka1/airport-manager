package cz.muni.fi.pa165.reporter.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public class ReporterDto {
    private Long reportId;
    private Long flightId;
    @Schema(example= "00:00:00", description = "Departure delay")
    private String departureDelay;
    @Schema(example= "00:00:00", description = "Arrival delay")
    private String arrivalDelay;
    private String message;
}
