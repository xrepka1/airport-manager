package cz.muni.fi.pa165.reporter.repository;

import cz.muni.fi.pa165.reporter.entity.Reporter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalTime;
import java.util.List;


@Repository
public interface ReporterRepository extends JpaRepository<Reporter, Long> {

    @Query("SELECT r FROM Reporter r WHERE r.departureDelay >= :departureDelay")
    List<Reporter> findAllByDepartureDelayOver(@Param("departureDelay") LocalTime departureDelay);

    @Query("SELECT r FROM Reporter r WHERE r.arrivalDelay >= :arrivalDelay")
    List<Reporter> findAllByArrivalDelayOver(@Param("arrivalDelay") LocalTime arrivalDelay);

    @Query("SELECT r FROM Reporter r WHERE r.flightId = :flightId")
    Reporter findByFlightId(@Param("flightId") Long flightId);
}
