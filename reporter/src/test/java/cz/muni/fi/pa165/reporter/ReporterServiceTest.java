package cz.muni.fi.pa165.reporter;

import cz.muni.fi.pa165.reporter.entity.Reporter;
import cz.muni.fi.pa165.reporter.repository.ReporterRepository;
import cz.muni.fi.pa165.reporter.service.ReporterService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
@DataJpaTest
public class ReporterServiceTest {
    @Mock
    private ReporterRepository reporterRepository;

    @InjectMocks
    private ReporterService service;

    private final Reporter reporter = Reporter.builder()
            .reportId(0L)
            .flightId(1L)
            .departureDelay(LocalTime.of(0, 30, 0))
            .arrivalDelay(LocalTime.of(0, 10,0))
            .message("This is report 1")
            .build();
    private final Reporter reporterEmpty = new Reporter();

    @BeforeEach
    public void setup() {
        service = new ReporterService(reporterRepository);
        assertEquals(0L, reporter.getReportId());
    }

    @Test
    public void testCreateReport() {
        when(reporterRepository.save(any(Reporter.class))).thenReturn(reporter);

        Reporter createdReporter = service.createReporter(reporter);
        assertNotNull(createdReporter);
        assertEquals(createdReporter.getReportId(), reporter.getReportId());
        assertEquals(createdReporter.getFlightId(), reporter.getFlightId());
        assertEquals(createdReporter.getDepartureDelay(), reporter.getDepartureDelay());
        assertEquals(createdReporter.getArrivalDelay(), reporter.getArrivalDelay());
        assertEquals(createdReporter.getMessage(), reporter.getMessage());
    }

    @Test
    public void testGetAllReports() {
        when(reporterRepository.findAll()).thenReturn(List.of(reporter, reporterEmpty));

        List<Reporter> allReporters = service.getAllReporters();
        assertNotNull(allReporters);
        assertEquals(allReporters.size(), 2);
        assertEquals(allReporters.get(0), reporter);
        assertEquals(allReporters.get(1), reporterEmpty);
    }

    @Test
    public void testFindById() {
        when(reporterRepository.findById(0L)).thenReturn(Optional.of(reporter));

        Reporter reporterById = service.getReporterById(0L);
        assertNotNull(reporterById);
        assertEquals(reporterById, reporter);
    }

    @Test
    public void testFindById2() {
        when(reporterRepository.findById(1L)).thenReturn(Optional.of(reporterEmpty));

        Reporter reporterById = service.getReporterById(1L);
        assertNotNull(reporterById);
        assertEquals(reporterById, reporterEmpty);
    }

    @Test
    public void testUpdateFlight() {
        Reporter updatedFlight = Reporter.builder()
                .reportId(0L)
                .flightId(1L)
                .departureDelay(LocalTime.of(0, 0, 0))
                .arrivalDelay(LocalTime.of(0, 10,0))
                .message("This is report 1")
                .build();

        when(reporterRepository.findById(0L)).thenReturn(Optional.of(reporter));
        when(reporterRepository.save(any(Reporter.class))).thenReturn(updatedFlight);

        Reporter resultedFlight = service.updateReport(0L, updatedFlight);

        assertEquals(updatedFlight.getFlightId(), resultedFlight.getFlightId());
        assertEquals(updatedFlight.getMessage(), resultedFlight.getMessage());
        assertEquals(updatedFlight.getArrivalDelay(), resultedFlight.getArrivalDelay());
    }

    @Test
    public void testRemoveFlightById() {
        doNothing().when(reporterRepository).deleteById(reporterEmpty.getReportId());

        service.removeReporterById(reporterEmpty.getReportId());
        verify(reporterRepository, times(1)).deleteById(reporterEmpty.getReportId());
    }
}
