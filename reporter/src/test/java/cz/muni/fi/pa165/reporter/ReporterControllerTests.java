package cz.muni.fi.pa165.reporter;

import cz.muni.fi.pa165.reporter.controller.ReporterController;
import cz.muni.fi.pa165.reporter.entity.Reporter;
import cz.muni.fi.pa165.reporter.entity.ReporterDto;
import cz.muni.fi.pa165.reporter.mapper.ReporterMapper;
import cz.muni.fi.pa165.reporter.service.ReporterService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.nio.charset.Charset;
import java.time.LocalTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class ReporterControllerTests {
    @InjectMocks
    private ReporterController reporterController;

    @Mock
    private ReporterService service;
    @Mock
    private ReporterMapper mapper;
    private Reporter reporter1;
    private Reporter reporter2;
    private ReporterDto reporter1Dto;
    private ReporterDto reporter2Dto;

    @BeforeEach
    public void setUp() {
        reporter1 = Reporter.builder()
                .reportId(1L)
                .flightId(1L)
                .departureDelay(LocalTime.of(0, 30, 0))
                .arrivalDelay(LocalTime.of(0, 10,0))
                .message("This is report 1")
                .build();
        reporter1Dto = new ReporterDto(1L, 1L, "00:30:00", "00:10:00", "This is report 1");

        reporter2 = Reporter.builder()
                .reportId(2L)
                .flightId(2L)
                .departureDelay(LocalTime.of(0, 30, 0))
                .arrivalDelay(LocalTime.of(1, 0,0))
                .message("This is report 2")
                .build();
        reporter2Dto = new ReporterDto(2L, 2L, "00:30:00", "01:00:00", "This is report 2");

        lenient().when(service.getAllReporters()).thenReturn(List.of(reporter1, reporter2));
        lenient().when(service.getReporterById(1L)).thenReturn((reporter1));
        lenient().when(service.createReporter(reporter1)).thenReturn(reporter1);
        lenient().when(service.updateReport(1L, reporter1)).thenReturn(reporter1);
        lenient().when(service.getReporterByFlightId(1L)).thenReturn(reporter1);
        lenient().when(service.getReportersWithDepartureDelayOver(LocalTime.parse("00:10:00"))).thenReturn(List.of(reporter1, reporter2));
        lenient().when(service.getReportersWithArrivalDelayOver(LocalTime.parse("00:30:00"))).thenReturn(List.of(reporter2));
        lenient().when(service.getReportersByQuery("departureDelayMin=00:10:00,departureDelayMax=01:00:00")).thenReturn(List.of(reporter1));

        lenient().when(mapper.toDto(reporter1)).thenReturn(reporter1Dto);
        lenient().when(mapper.toDto(reporter2)).thenReturn(reporter2Dto);
        lenient().when(mapper.toEntity(reporter1Dto)).thenReturn(reporter1);
        lenient().when(mapper.toEntity(reporter2Dto)).thenReturn(reporter2);


    }

    @Test
    public void testGetAllReporters() {
        ResponseEntity<List<ReporterDto>> responseEntity = reporterController.getAllReporters();
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        List<ReporterDto> allReporters = responseEntity.getBody();
        assertEquals(2, allReporters.size());
        assertEquals(List.of(reporter1Dto, reporter2Dto), responseEntity.getBody());
    }

    @Test
    public void testGetReporterById() {
        ResponseEntity<ReporterDto> responseEntity = reporterController.getReporterById(1L);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(reporter1Dto, responseEntity.getBody());
    }

    @Test
    public void testCreateReporter() {
        ResponseEntity<ReporterDto> responseEntity = reporterController.createReporter(reporter1Dto);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(reporter1Dto, responseEntity.getBody());
    }

    @Test
    public void testUpdateReporter() {
        ResponseEntity<ReporterDto> responseEntity = reporterController.updateReporter(1L, reporter1Dto);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(reporter1Dto, responseEntity.getBody());
    }

    @Test
    public void testDeleteReporter() {
        ResponseEntity<?> response = reporterController.deleteReporter(1L);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }
    
    @Test
    public void testGetReporterByFlightId() {
        ResponseEntity<ReporterDto> responseEntity = reporterController.getReporterByFlightId(1L);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(reporter1Dto, responseEntity.getBody());
    }
    
    @Test
    public void testGetReportersWithDepartureDelayOver() {
        ResponseEntity<List<ReporterDto>> responseEntity = reporterController.getReportersWithDepartureDelayOver("00:10:00");
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(List.of(reporter1Dto, reporter2Dto), responseEntity.getBody());
    }

    @Test
    public void testGetReportersWithArrivalDelayOver() {
        ResponseEntity<List<ReporterDto>> responseEntity = reporterController.getReportersWithArrivalDelayOver("00:30:00");
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(List.of(reporter2Dto), responseEntity.getBody());
    }

    @Test
    public void testDownloadReportersFileByQuerySuccess() {
        String fileContent = List.of(reporter1).toString();
        try {
            ResponseEntity<?> responseEntity = reporterController.downloadReportersFileByQuery("departureDelayMin=00:10:00,departureDelayMax=01:00:00");
            assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
            assertEquals(UrlResource.class, responseEntity.getBody().getClass());
            Resource responseBody = (Resource) responseEntity.getBody();
            assertEquals("reporter.txt", responseBody.getFilename());
            assertEquals(fileContent, responseBody.getContentAsString(Charset.defaultCharset()));
        }
        catch (IOException e) {
            fail();
        }
    }
}