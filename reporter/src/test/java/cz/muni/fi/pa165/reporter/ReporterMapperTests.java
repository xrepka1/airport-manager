package cz.muni.fi.pa165.reporter;

import cz.muni.fi.pa165.reporter.entity.Reporter;
import cz.muni.fi.pa165.reporter.entity.ReporterDto;
import cz.muni.fi.pa165.reporter.mapper.ReporterMapper;
import cz.muni.fi.pa165.reporter.mapper.ReporterMapperImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class ReporterMapperTests {

    @InjectMocks
    private ReporterMapper mapper = new ReporterMapperImpl();
    private Reporter reporter;
    private ReporterDto reporterDto;

    @BeforeEach
    public void setup() {
        reporter = Reporter.builder()
                .reportId(1L)
                .flightId(1L)
                .departureDelay(LocalTime.of(0, 30, 0))
                .arrivalDelay(LocalTime.of(0, 10,0))
                .message("This is report 1")
                .build();
        assertEquals(1L, reporter.getReportId());
        reporterDto = new ReporterDto(1L, 1L, "00:30:00", "00:10:00", "This is report 1");

    }

    @Test
    public void testToDto() {
        var resultDto = mapper.toDto(reporter);

        assertEquals(reporter.getReportId(), resultDto.getReportId());
        assertEquals(reporter.getMessage(), resultDto.getMessage());
        assertEquals(reporter.getFlightId(), resultDto.getFlightId());
        assertEquals(reporter.getDepartureDelay(), LocalTime.parse(resultDto.getDepartureDelay()));
        assertEquals(reporter.getArrivalDelay(), LocalTime.parse(resultDto.getArrivalDelay()));
    }

    @Test
    public void testToEntity() {
        var resultEntity = mapper.toEntity(reporterDto);

        assertEquals(reporterDto.getReportId(), resultEntity.getReportId());
        assertEquals(reporterDto.getMessage(), resultEntity.getMessage());
        assertEquals(reporterDto.getFlightId(), resultEntity.getFlightId());
        assertEquals(LocalTime.parse(reporterDto.getDepartureDelay()), resultEntity.getDepartureDelay());
        assertEquals(LocalTime.parse(reporterDto.getArrivalDelay()), resultEntity.getArrivalDelay());
    }
}
