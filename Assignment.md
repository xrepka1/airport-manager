Milestone 3
---

- [X] The REST API developed in M1 needs to be connected to the service and persistence layer developed in M2 for all the microservices (depending on the number of team members remaining in your team this might apply to a more limited set of services);
- [ ] Set up OAuth 2 Resource Server protection using Spring Security for your API. 
  - [ ] Reuse the client_id and client_secret for both the client and resource servers, and the test_* scopes from Seminar 09. Use different scopes for different methods where it makes sense. 
  - [ ] Use at least 2 different scopes (Task 7 Seminar 9)
  - [ ] Messenger
  - [ ] Reporter
  - [ ] Flight Management
  - [ ] Staff Management
  - (The client is registered for the following redirect URLs:
  - http://localhost:8080/login/oauth2/code/muni http://localhost:8081/login/oauth2/code/muni http://localhost:8082/login/oauth2/code/muni http://localhost:8083/login/oauth2/code/muni http://localhost:8084/login/oauth2/code/muni 
  - (Not required) http://localhost:8090/swagger-ui/oauth2-redirect.html
  - Use the client just to get an access token, no need to implement the UI);
- [ ] Each service must expose actuator health, metrics;
  - [ ] Your systems will collect the metrics in an automated way (e.g., Prometheus);
  - [ ] You must have a monitoring dashboard (e.g., Grafana) showing the state of your system;
  - [ ] Messenger
  - [ ] Reporter
  - [ ] Flight Management
  - [ ] Staff Management
- [ ] Define at least one runnable scenario to showcase the system: think about simulated number of users accessing the  services with some specific goals 
  - [ ] Describe in the README the scenario and the goals (e.g., showcasing high loads on one service during peak times) and what are the expected results;
  - You can do this by implementing a script or using tools such as locust.io (https://locust.io/), JMeter (https://jmeter.apache.org/), or any platform that can be useful to represent users' behaviour;
- [ ] The README must contain all the instructions to run the application and the provided runnable scenario (i.e. for a script: the script itself and instructions on how to run it);
- [ ] There must be a way to seed the databases of your microservices. The data should also look somewhat realistic (no "AAAAAA" for names etc.)
- [ ] Merge requests now require Approval of at least 2 members (set up your GitLab this way), the idea of using merge requests is not to create a branch, push code and merge to master. It should establish common code style and help code understanding within the team.

Milestone 2
---

- [X] Implement the persistence layer for your microservices;
  - [X] flight-management
  - [X] messenger
  - [X] reporter
  - [X] resource-management
- [X] You can use an in-memory database;
  - [X] flight-management
  - [X] messenger
  - [X] reporter
  - [X] resource-management
- [ ] Implement at least two non-trivial business functionalities that go beyond CRUD operations;
  - [ ] Send email to staff member
  - [X] Generate report in txt form
  - [X] Get layover functionality
- [ ] There must be extensive unit tests for each microservice (particularly for your business functions) and the tests of the service layer must use mocked repository objects;
  - [ ] flight-management
  - [ ] messenger
  - [ ] reporter
  - [ ] resource-management
- [X] Create at least $n entity classes for your project where $n is the number of team members;
  - [X] plane
  - [X] staff
  - [X] flight
  - [X] destination
- [X] Create Spring repository interfaces;
  - [X] flight-management
  - [X] messenger
  - [X] reporter
  - [X] resource-management
- [ ] Implement at least 
  - [X] two custom methods in repository interfaces and 
  - [ ] test all methods thoroughly;
- [X] Each team member should work with different entities on different parts of the project (e.g. member 1 will create entity A, but member 2 will create tests for entity A)
- [X] Implement and use a GitLab pipeline for CI to be notified about failing builds and tests;
- [X] Configure and use Podman and Podman Compose to package and run your services (Docker is also accepted). Place all the necessary commands to build and run your system into the project README;
- [X] Please note that the REST API developed in M1 needs to be connected to the service and persistence layer developed in M2 for at least one microservice: you will complete for all microservices in M3 but you might want to already do for all of the microservices in this milestone;

Milestone 1
---

- [X] Analyze the domain and design the system with at least 4 services.
- [X] Implement the services as at least 4 Spring Boot applications each in a separate Maven module.
- [X] Each student creates a skeleton for one service (that is create the REST controllers).
- [X] Annotate the Java classes to generate OpenAPI specifications.
- [ ] Create tests for each service (one student takes responsibility of a test class for one different service).
- [X] Externalize configuration using Spring Boot’s application.yml file (doc).
- [ ] Use validation of inputs for the DTOs.
- [X] Show that you have been using pull requests in this milestone.
- [X] The team leader will be responsible for managing pull requests.
- [X] The project must be buildable using Maven through a command line: make sure you have all dependencies correctly configured, so it will be possible to run “mvn clean install“ and “mvn spring-boot:run” in executable modules.
- [X] List the executable modules in the README.

Assignment
---

Create an information system managing flight records at an airport.

The system should allow the users to
- create,
- update,
- delete records about
  - stewards,
  - airplanes,
  - destinations
  - flights

A destination should contain at least the information about the location of an airport
- country,
- city
- airport
- (possibly airport code)

Airplane details should contain the
- capacity of the plane and its
- name
- (and possibly its type as well).

A steward is described by his
- first and
- last name.

The system should also allow us to record a flight.
When recording a flight, it is necessary to set the
- departure and
- arrival times, the
- origin, the
- destination and the
- plane.

The system should also
- check that the plane does not have another flight scheduled during the time of the this flight.

It should also be possible to
- assign,
- remove stewards
  - to
  - from a flight while checking for the steward's availability.

The ultimate goal is to have a system capable of
- listing flights
  - ordered by date and
  - displaying the flight details
    - origin,
    - destination,
    - departure time,
    - arrival time,
    - plane,
    - list of stewards
